<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MProgramStudi;

class MasterProgramStudiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $prodi = [
            ['id' => 1, 'name' => 'Data Science', 'm_jurusan_id' => 1],
            ['id' => 2, 'name' => 'Sistem Informasi', 'm_jurusan_id' => 1],

            ['id' => 3, 'name' => 'Pendidikan Matematika', 'm_jurusan_id' => 2],
            ['id' => 4, 'name' => 'Matematika Murni', 'm_jurusan_id' => 2],
            ['id' => 5, 'name' => 'Pendidikan Fisika', 'm_jurusan_id' => 3],
            ['id' => 6, 'name' => 'Fisika Murni', 'm_jurusan_id' => 3],
            ['id' => 7, 'name' => 'Pendidikan Kimia', 'm_jurusan_id' => 4],
            ['id' => 8, 'name' => 'Kimia Murni', 'm_jurusan_id' => 4],
            ['id' => 9, 'name' => 'Hukum Pidana', 'm_jurusan_id' => 5],
            ['id' => 10, 'name' => 'Hukum Perdata', 'm_jurusan_id' => 5],

        ];

        MProgramStudi::insert($prodi);
        // $this->call("OthersTableSeeder");
    }
}
