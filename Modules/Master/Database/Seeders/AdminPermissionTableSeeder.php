<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class AdminPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $permissions = [
            ['name' => 'master'],
                ['name' => 'master-user'],
                    ['name' => 'master-user-gender'],
                            ['name' => 'master-user-gender-index'],
                            ['name' => 'master-user-gender-create'],
                            ['name' => 'master-user-gender-store'],
                            ['name' => 'master-user-gender-show'],
                            ['name' => 'master-user-gender-edit'],
                            ['name' => 'master-user-gender-update'],
                            ['name' => 'master-user-gender-delete'],
                            ['name' => 'master-user-gender-approve'],
                        ['name' => 'master-user-user type'],
                            ['name' => 'master-user-user type-index'],
                            ['name' => 'master-user-user type-create'],
                            ['name' => 'master-user-user type-store'],
                            ['name' => 'master-user-user type-show'],
                            ['name' => 'master-user-user type-edit'],
                            ['name' => 'master-user-user type-update'],
                            ['name' => 'master-user-user type-delete'],
                            ['name' => 'master-user-user type-approve'],
                    ['name' => 'master-user-mahasiswa'],
                        ['name' => 'master-user-mahasiswa-fakultas'],
                            ['name' => 'master-user-mahasiswa-fakultas-index'],
                            ['name' => 'master-user-mahasiswa-fakultas-create'],
                            ['name' => 'master-user-mahasiswa-fakultas-store'],
                            ['name' => 'master-user-mahasiswa-fakultas-show'],
                            ['name' => 'master-user-mahasiswa-fakultas-edit'],
                            ['name' => 'master-user-mahasiswa-fakultas-update'],
                            ['name' => 'master-user-mahasiswa-fakultas-delete'],
                            ['name' => 'master-user-mahasiswa-fakultas-approve'],
                        ['name' => 'master-user-mahasiswa-jurusan'],
                            ['name' => 'master-user-mahasiswa-jurusan-index'],
                            ['name' => 'master-user-mahasiswa-jurusan-create'],
                            ['name' => 'master-user-mahasiswa-jurusan-store'],
                            ['name' => 'master-user-mahasiswa-jurusan-show'],
                            ['name' => 'master-user-mahasiswa-jurusan-edit'],
                            ['name' => 'master-user-mahasiswa-jurusan-update'],
                            ['name' => 'master-user-mahasiswa-jurusan-delete'],
                            ['name' => 'master-user-mahasiswa-jurusan-approve'],
                        ['name' => 'master-user-mahasiswa-program studi'],
                            ['name' => 'master-user-mahasiswa-program studi-index'],
                            ['name' => 'master-user-mahasiswa-program studi-create'],
                            ['name' => 'master-user-mahasiswa-program studi-store'],
                            ['name' => 'master-user-mahasiswa-program studi-show'],
                            ['name' => 'master-user-mahasiswa-program studi-edit'],
                            ['name' => 'master-user-mahasiswa-program studi-update'],
                            ['name' => 'master-user-mahasiswa-program studi-delete'],
                            ['name' => 'master-user-mahasiswa-program studi-approve'],
                        ['name' => 'master-user-mahasiswa-tingkat pendidikan'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-index'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-create'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-store'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-show'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-edit'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-update'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-delete'],
                            ['name' => 'master-user-mahasiswa-tingkat pendidikan-approve'],
                    ['name' => 'master-user-mitra unpad'],
                        ['name' => 'master-user-mitra unpad-jabatan'],
                            ['name' => 'master-user-mitra unpad-jabatan-index'],
                            ['name' => 'master-user-mitra unpad-jabatan-create'],
                            ['name' => 'master-user-mitra unpad-jabatan-store'],
                            ['name' => 'master-user-mitra unpad-jabatan-show'],
                            ['name' => 'master-user-mitra unpad-jabatan-edit'],
                            ['name' => 'master-user-mitra unpad-jabatan-update'],
                            ['name' => 'master-user-mitra unpad-jabatan-delete'],
                            ['name' => 'master-user-mitra unpad-jabatan-approve'],
                        ['name' => 'master-user-mitra unpad-jenis perusahaan'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-index'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-create'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-store'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-show'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-edit'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-update'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-delete'],
                            ['name' => 'master-user-mitra unpad-jenis perusahaan-approve'],
                        ['name' => 'master-user-mitra unpad-perusahaan'],
                            ['name' => 'master-user-mitra unpad-perusahaan-index'],
                            ['name' => 'master-user-mitra unpad-perusahaan-create'],
                            ['name' => 'master-user-mitra unpad-perusahaan-store'],
                            ['name' => 'master-user-mitra unpad-perusahaan-show'],
                            ['name' => 'master-user-mitra unpad-perusahaan-edit'],
                            ['name' => 'master-user-mitra unpad-perusahaan-update'],
                            ['name' => 'master-user-mitra unpad-perusahaan-delete'],
                            ['name' => 'master-user-mitra unpad-perusahaan-approve'],
                ['name' => 'master-address'],
                    ['name' => 'master-address-province'],
                    ['name' => 'master-address-city'],
                ['name' => 'master-ticketing'],
                    ['name' => 'master-ticketing-kategori'],
                        ['name' => 'master-ticketing-kategori-index'],
                        ['name' => 'master-ticketing-kategori-create'],
                        ['name' => 'master-ticketing-kategori-store'],
                        ['name' => 'master-ticketing-kategori-show'],
                        ['name' => 'master-ticketing-kategori-edit'],
                        ['name' => 'master-ticketing-kategori-update'],
                        ['name' => 'master-ticketing-kategori-delete'],
                        ['name' => 'master-ticketing-kategori-approve'],
                ['name' => 'Feature-ticketing'],
                    ['name' => 'ticketing'],
                        ['name' => 'ticketing-index'],
                        ['name' => 'ticketing-show'],
                        ['name' => 'ticketing-edit'],
                        ['name' => 'ticketing-update'],
                        ['name' => 'ticketing-delete'],
                        ['name' => 'ticketing-approve'],
        ];

        foreach ($permissions as $permission) {
            # code...
            Permission::create($permission);
        }
        // $this->call("OthersTableSeeder");
    }
}
