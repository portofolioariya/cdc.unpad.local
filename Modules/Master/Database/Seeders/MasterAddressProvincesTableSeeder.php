<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MProvince;

class MasterAddressProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $provinces = [
            ['id' => '11', 'name' => 'ACEH', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '12', 'name' => 'SUMATERA UTARA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '13', 'name' => 'SUMATERA BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '14', 'name' => 'RIAU', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '15', 'name' => 'JAMBI', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '16', 'name' => 'SUMATERA SELATAN', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '17', 'name' => 'BENGKULU', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '18', 'name' => 'LAMPUNG', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '19', 'name' => 'KEPULAUAN BANGKA BELITUNG', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '21', 'name' => 'KEPULAUAN RIAU', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '31', 'name' => 'DKI JAKARTA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '32', 'name' => 'JAWA BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '33', 'name' => 'JAWA TENGAH', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '34', 'name' => 'DI YOGYAKARTA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '35', 'name' => 'JAWA TIMUR', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '36', 'name' => 'BANTEN', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '51', 'name' => 'BALI', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '52', 'name' => 'NUSA TENGGARA BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '53', 'name' => 'NUSA TENGGARA TIMUR', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '61', 'name' => 'KALIMANTAN BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '62', 'name' => 'KALIMANTAN TENGAH', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '63', 'name' => 'KALIMANTAN SELATAN', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '64', 'name' => 'KALIMANTAN TIMUR', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '65', 'name' => 'KALIMANTAN UTARA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '71', 'name' => 'SULAWESI UTARA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '72', 'name' => 'SULAWESI TENGAH', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '73', 'name' => 'SULAWESI SELATAN', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '74', 'name' => 'SULAWESI TENGGARA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '75', 'name' => 'GORONTALO', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '76', 'name' => 'SULAWESI BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '81', 'name' => 'MALUKU', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '82', 'name' => 'MALUKU UTARA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '91', 'name' => 'PAPUA BARAT', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
            ['id' => '94', 'name' => 'PAPUA', 'created_at' => '2020-11-22 02:50:54', 'updated_at' => '2020-11-22 02:50:54', 'created_by' => '1', ],
        ];
        MProvince::insert($provinces);
        // $this->call("OthersTableSeeder");
    }
}
