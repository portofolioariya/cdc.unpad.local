<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MTingkatPendidikan;

class MasterTingkatPendidikanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $tingkat_pendidikan = [
            ['name' => 'S1'],
            ['name' => 'S2'],
            ['name' => 'S3'],
        ];
        MTingkatPendidikan::insert($tingkat_pendidikan);
        // $this->call("OthersTableSeeder");
    }
}
