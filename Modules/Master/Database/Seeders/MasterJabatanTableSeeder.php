<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MJabatan;

class MasterJabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $jabatan = [
            ['id' => 1, 'name' => 'Web Developer'],
            ['id' => 2, 'name' => 'Manager'],
            ['id' => 3, 'name' => 'Freelancer'],
        ];

        MJabatan::insert($jabatan);
        // $this->call("OthersTableSeeder");
    }
}
