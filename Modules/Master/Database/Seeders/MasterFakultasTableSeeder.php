<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MFakultas;

class MasterFakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $fakultas = [
            ['id' => 1, 'name' => 'Fakultas Teknik dan Ilmu Komputer'],
            ['id' => 2, 'name' => 'Fakultas Matematika Ilmu Pengetahuan Alam'],
            ['id' => 3, 'name' => 'Fakultas Sosial Politik'],
        ];

        MFakultas::insert($fakultas);
        // $this->call("OthersTableSeeder");
    }
}
