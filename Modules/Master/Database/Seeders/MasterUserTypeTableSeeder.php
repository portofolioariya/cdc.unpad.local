<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MUserType;

class MasterUserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $user_type = [
            ['name' => 'mahasiswa'],
            ['name' => 'mitra unpad'],
            ['name' => 'admin'],
        ];
        MUserType::insert($user_type);
        // $this->call("OthersTableSeeder");
    }
}
