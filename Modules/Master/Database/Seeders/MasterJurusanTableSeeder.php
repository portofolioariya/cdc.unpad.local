<?php

namespace Modules\Master\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Master\Entities\MJurusan;

class MasterJurusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $jurusan = [
            ['id' => 1, 'name' => 'Teknik Informatika', 'm_fakultas_id' => 1],

            ['id' => 2, 'name' => 'Matematika', 'm_fakultas_id' => 2],
            ['id' => 3, 'name' => 'Fisika', 'm_fakultas_id' => 2],
            ['id' => 4, 'name' => 'Kimia', 'm_fakultas_id' => 2],
            ['id' => 5, 'name' => 'Hukum', 'm_fakultas_id' => 3],
        ];

        MJurusan::insert($jurusan);
        // $this->call("OthersTableSeeder");
    }
}
