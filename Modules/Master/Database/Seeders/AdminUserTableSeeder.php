<?php

namespace Modules\Master\Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Modules\Master\Entities\MUserType;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $first_name = 'Super';
        $last_name = 'Admin';
        $email =  'admin@gmail.com';
        $username = 'admin';
        $password = '12345678';
        $created = Carbon::now();

        $user_admin = User::create([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' =>  $username,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
        $role_admin = Role::find(1);
        $user_admin->assignRole($role_admin);
        $user_type_admin = MUserType::find(3);
        $user_admin->user_type()->attach([$user_type_admin->id]);

        $user_admin->profile()->create([
            'm_province_id' => null,
            'm_city_id' => null,
            'm_gender_id' => null,
            'images' => null,
            'no_telp' => null,
            'tgl_lahir' => null,
            'address' => null,
            'status_pekerjaan' => null,
            'status_pernikahan' => null,
            'about_me' => null,
            'created_by' => $user_admin->id,
        ]);

        $permissions_admin = Permission::all();
        foreach ($permissions_admin as $permission) {
            # code...
            $role_admin->givePermissionTo($permission);
            $user_admin->givePermissionTo($permission);
        }
        // $this->call("OthersTableSeeder");
    }
}
