<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMProgramStudisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_program_studis', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_jurusan_id')->unsigned();
            $table->foreign('m_jurusan_id')->references('id')->on('m_jurusans')->onDelete('cascade');
            $table->string('name');
            $table->bigInteger('feature_code')->nullable();
            $table->enum('approved_status',['confirmed', 'unconfirmed', 'pending'])->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_program_studis');
    }
}
