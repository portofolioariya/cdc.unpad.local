<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPerusahaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_perusahaans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_jenis_perusahaan_id')->unsigned();
            $table->foreign('m_jenis_perusahaan_id')->references('id')->on('m_jenis_perusahaans')->onDelete('cascade');
            $table->bigInteger('m_city_id')->unsigned();
            $table->foreign('m_city_id')->references('id')->on('m_cities')->onDelete('cascade');
            $table->string('images');
            $table->string('name');
            $table->string('email');
            $table->bigInteger('no_tlp');
            $table->string('description')->nullable();
            $table->bigInteger('feature_code')->nullable();
            $table->enum('approved_status',['confirmed', 'unconfirmed', 'pending'])->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_perusahaans');
    }
}
