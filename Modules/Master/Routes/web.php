<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\MFakultasController;
use Modules\Master\Http\Controllers\MGenderController;
use Modules\Master\Http\Controllers\MJabatanController;
use Modules\Master\Http\Controllers\MJenisPerusahaanController;
use Modules\Master\Http\Controllers\MTicketingController;
use Modules\Master\Http\Controllers\MCityController;
use Modules\Master\Http\Controllers\MProvinceController;
use Modules\Master\Http\Controllers\MPerusahaanController;
use Modules\Master\Http\Controllers\MJurusanController;
use Modules\Master\Http\Controllers\MProgramStudiController;
use Modules\Master\Http\Controllers\MTingkatPendidikanController;
use Modules\Master\Http\Controllers\MUserTypeController;

Route::group(['middleware' => 'auth'], function (){
    Route::group(['middleware' => ['permission:master-user-gender']], function (){
        Route::prefix('master-user-gender')->group(function() {
            Route::group(['middleware' => ['permission:master-user-gender-index']], function (){
                Route::get('/', [MGenderController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-user-gender-store']], function (){
                Route::post('/', [MGenderController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-user-gender-update']], function (){
                Route::put('/{id}', [MGenderController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-gender-delete']], function (){
                Route::delete('/{id}', [MGenderController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-gender-approve']], function (){
                Route::put('/{id}/approve', [MGenderController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-user type']], function (){
        Route::prefix('master-user-user-type')->group(function() {
            Route::group(['middleware' => ['permission:master-user-user type-index']], function (){
                Route::get('/', [MUserTypeController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-user-user type-store']], function (){
                Route::post('/', [MUserTypeController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-user-user type-update']], function (){
                Route::put('/{id}', [MUserTypeController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-user type-delete']], function (){
                Route::delete('/{id}', [MUserTypeController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-user type-approve']], function (){
                Route::put('/{id}/approve', [MUserTypeController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan']], function (){
        Route::prefix('master-user-mitra-unpad-jabatan')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan-index']], function (){
                Route::get('/', [MJabatanController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan-store']], function (){
                Route::post('/', [MJabatanController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan-update']], function (){
                Route::put('/{id}', [MJabatanController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan-delete']], function (){
                Route::delete('/{id}', [MJabatanController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jabatan-approve']], function (){
                Route::put('/{id}/approve', [MJabatanController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan']], function (){
        Route::prefix('master-user-mitra-unpad-jenis-perusahaan')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan-index']], function (){
                Route::get('/', [MJenisPerusahaanController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan-store']], function (){
                Route::post('/', [MJenisPerusahaanController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan-update']], function (){
                Route::put('/{id}', [MJenisPerusahaanController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan-delete']], function (){
                Route::delete('/{id}', [MJenisPerusahaanController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-jenis perusahaan-approve']], function (){
                Route::put('/{id}/approve', [MJenisPerusahaanController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan']], function (){
        Route::prefix('master-user-mitra-unpad-perusahaan')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan-index']], function (){
                Route::get('/', [MPerusahaanController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan-store']], function (){
                Route::post('/', [MPerusahaanController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan-update']], function (){
                Route::put('/{id}', [MPerusahaanController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan-delete']], function (){
                Route::delete('/{id}', [MPerusahaanController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mitra unpad-perusahaan-approve']], function (){
                Route::put('/{id}/approve', [MPerusahaanController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-ticketing']], function (){
        Route::prefix('master-ticketing-kategori')->group(function() {
            Route::group(['middleware' => ['permission:master-ticketing-kategori-index']], function (){
                Route::get('/', [MTicketingController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:master-ticketing-kategori-store']], function (){
                Route::post('/', [MTicketingController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:master-ticketing-kategori-update']], function (){
                Route::put('/{id}', [MTicketingController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-ticketing-kategori-delete']], function (){
                Route::delete('/{id}', [MTicketingController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-ticketing-kategori-approve']], function (){
                Route::put('/{id}/approve', [MTicketingController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas']], function (){
        Route::prefix('master-user-mahasiswa-fakultas')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-index']], function (){
                Route::get('/', [MFakultasController::class, 'index']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-create']], function (){
            //     Route::get('/create', [MFakultasController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-store']], function (){
                Route::post('/', [MFakultasController::class, 'store']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-show']], function (){
            //     Route::get('/{id}', [MFakultasController::class, 'index']);
            // });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-edit']], function (){
            //     Route::get('/{id}/edit', [MFakultasController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-update']], function (){
                Route::put('/{id}', [MFakultasController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-delete']], function (){
                Route::delete('/{id}', [MFakultasController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-fakultas-approve']], function (){
                Route::put('/{id}/approve', [MFakultasController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-address']], function (){
        Route::prefix('master-address-province')->group(function() {
            Route::get('/', [MProvinceController::class, 'index']);
        });

        Route::prefix('master-address-city')->group(function() {
            Route::get('/', [MCityController::class, 'index']);
        });
    });

    Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan']], function (){
        Route::prefix('master-user-mahasiswa-jurusan')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-index']], function (){
                Route::get('/', [MJurusanController::class, 'index']);
                Route::post('/', [MJurusanController::class, 'getJurusan']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-create']], function (){
            //     Route::get('/create', [MJurusanController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-store']], function (){
                Route::post('/', [MJurusanController::class, 'store']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-show']], function (){
            //     Route::get('/{id}', [MJurusanController::class, 'index']);
            // });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-edit']], function (){
            //     Route::get('/{id}/edit', [MJurusanController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-update']], function (){
                Route::put('/{id}', [MJurusanController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-delete']], function (){
                Route::delete('/{id}', [MJurusanController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-jurusan-approve']], function (){
                Route::put('/{id}/approve', [MJurusanController::class, 'approve']);
            });
        });
    });


    Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi']], function (){
        Route::prefix('master-user-mahasiswa-program-studi')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-index']], function (){
                Route::get('/', [MProgramStudiController::class, 'index']);
                Route::get('/getJurusan', [MProgramStudiController::class, 'getJurusan']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-create']], function (){
            //     Route::get('/create', [MProgramStudiController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-store']], function (){
                Route::post('/', [MProgramStudiController::class, 'store']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-show']], function (){
            //     Route::get('/{id}', [MProgramStudiController::class, 'index']);
            // });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-edit']], function (){
            //     Route::get('/{id}/edit', [MProgramStudiController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-update']], function (){
                Route::put('/{id}', [MProgramStudiController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-delete']], function (){
                Route::delete('/{id}', [MProgramStudiController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-program studi-approve']], function (){
                Route::put('/{id}/approve', [MProgramStudiController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan']], function (){
        Route::prefix('master-user-mahasiswa-tingkat-pendidikan')->group(function() {
            Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-index']], function (){
                Route::get('/', [MTingkatPendidikanController::class, 'index']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-create']], function (){
            //     Route::get('/create', [MTingkatPendidikanController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-store']], function (){
                Route::post('/', [MTingkatPendidikanController::class, 'store']);
            });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-show']], function (){
            //     Route::get('/{id}', [MTingkatPendidikanController::class, 'index']);
            // });
            // Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-edit']], function (){
            //     Route::get('/{id}/edit', [MTingkatPendidikanController::class, 'index']);
            // });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-update']], function (){
                Route::put('/{id}', [MTingkatPendidikanController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-delete']], function (){
                Route::delete('/{id}', [MTingkatPendidikanController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:master-user-mahasiswa-tingkat pendidikan-approve']], function (){
                Route::put('/{id}/approve', [MTingkatPendidikanController::class, 'approve']);
            });
        });
    });

});
