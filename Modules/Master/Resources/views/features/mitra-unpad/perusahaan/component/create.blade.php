<a data-bs-toggle="modal" data-bs-target="#createData" class="btn btn-sm btn-light-primary">
    Action
</a>

<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="createData">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Master Perusahaan</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
                <!--end::Close-->
            </div>

            <form action="{{URL('master-user-mitra-unpad-perusahaan')}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="modal-body">
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">Jenis Perusahaan</label>
                            <div class="mb-10" data-select2-id="select2-data-8-mi3k">
                                <select name="jenis_perusahaan" class="form-select select2-hidden-accessible" data-control="select2" data-placeholder="Select an option" data-select2-id="select2-data-4-nnqy" tabindex="-1" aria-hidden="true">
                                    <option data-select2-id="select2-data-6-4dsp"></option>
                                    @foreach($jenisPerusahaan as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('jenis_perusahaan')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="login" data-validator="login">
                                        {{ $message }}
                                    </div>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">Image</label>
                            <div class="mb-10">
                                <input type="file" name="image" class="form-control @error('image')
                                    is-invalid
                                @enderror" value="{{old('image')}}" />
                            </div>
                            @error('image')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">City</label>
                            <div class="mb-10" data-select2-id="select2-data-8-mi3k">
                                <select name="city" class="form-select" data-control="select2" data-placeholder="Select an option" >
                                    <option data-select2-id="select2-data-6-4dsp"></option>
                                    @foreach($city as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('city')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">Name</label>
                            <input type="text" name="name" class="form-control @error('name')
                                is-invalid
                            @enderror" placeholder="Please input name" value="{{old('name')}}" />
                            @error('name')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">E-mail</label>
                            <input type="email" name="email" class="form-control @error('email')
                                is-invalid
                            @enderror" placeholder="Please input email" value="{{old('email')}}" />
                            @error('email')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">No Telephone</label>
                            <input type="text" name="no_tlp" class="form-control @error('no_tlp')
                                is-invalid
                            @enderror" placeholder="Please input no telephone" value="{{old('no_tlp')}}" />
                            @error('no_tlp')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="name" class="required form-label">Description</label>
                            <textarea name="desc" class="form-control">
                                {{ old('desc') }}
                            </textarea>
                            @error('desc')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
