
<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="showData{{$val_perusahaan->id}}">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="required form-label">Image</label>
                    <br/>
                    <img class="mt-2 mb-3" src="{{ URL('storage/'.$val_perusahaan->images) }}" style="width: 100px">
                </div>
                <div class="form-group">
                    <label for="name" class="required form-label">Jenis Perusahaan</label>
                    <input type="text" class="form-control" placeholder="Please input name" value="{{old('name', $val_perusahaan->m_jenis_perusahaan->name)}}" disabled />
                </div>
                <div class="form-group mt-1">
                    <label for="name" class="required form-label">City</label>
                    <input type="text" class="form-control" placeholder="Please input name" value="{{old('name', $val_perusahaan->m_city->name)}}" disabled />
                </div>
                <div class="form-group">
                    <label for="name" class="required form-label">E-mail</label>
                    <input type="text" class="form-control" placeholder="Please input name" value="{{old('name', $val_perusahaan->email)}}" disabled />
                </div>
                <div class="form-group">
                    <label for="name" class="required form-label">No Telephone</label>
                    <input type="text" class="form-control" placeholder="Please input name" value="{{old('name', $val_perusahaan->no_tlp)}}" disabled />
                </div>
                <div class="form-group">
                    <label for="name" class="required form-label">Desc</label>
                    <textarea class="form-control" readonly>{{ $val_perusahaan->description }}</textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
