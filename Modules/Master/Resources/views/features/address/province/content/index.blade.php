@extends('layouts.base-layout.content.base-layout')
@push('title', 'Form Master Province')
@section('content')
<div>
    <div class="card shadow-sm mt-5">
        <div class="card-header">
            <h3 class="card-title">Master Province</h3>
            <div class="card-toolbar">
{{--                @include('master::features.mitra-unpad.jabatan.component.create')--}}
            </div>
        </div>
        {{-- <div class="mb-10">
            <select class="form-select select_group" data-control="select2" data-placeholder="Select an option">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
            </select>
        </div> --}}
        <div class="card-body">
            <table id="example" class="table table-striped text-center" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($province as $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$value->name}}</td>
                        <td>
                            <div class="d-flex justify-content-center">
                                <a data-bs-toggle="modal" data-bs-target="#showData{{$value->id}}" class="btn btn-icon btn-bg-light-secondary btn-active-color-secondary btn-sm me-5">
                                    <span class="svg-icon svg-icon-3">
                                        <img src="{{URL('assets/dist/assets/media/icons/duotune/general/gen004.svg')}}"/>
                                    </span>
                                </a>
                            </div>
                        </td>
                    </tr>
                    {{--                    @include('master::features.address.province.component.show')--}}
                    @endforeach
                </tbody>
                {{-- <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot> --}}
            </table>

        </div>
        {{-- <div class="card-footer">
            <button type="button" class="btn btn-sm btn-primary">
                Action
            </button>
        </div> --}}
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
