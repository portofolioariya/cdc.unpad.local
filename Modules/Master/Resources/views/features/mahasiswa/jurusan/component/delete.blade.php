<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="deleteData{{$jurusan->id}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Data</h5>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
                <!--end::Close-->
            </div>

            <form action="{{URL('master-user-mahasiswa-jurusan/'.\Illuminate\Support\Facades\Crypt::encrypt($jurusan->id))}}" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-body text-center">
                    <div class="nk-modal">
                        {{-- <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-trash-fill bg-danger"></em> --}}
                        <h4 class="nk-modal-title">Yakin ingin menghapus data ?</h4>
                        <div class="nk-modal-text">
                            <label for="">Name : {{$jurusan->name}}</label>
                            {{-- <p class="lead">
                                
                            </p> --}}
                        </div>
                        <div class="nk-modal-action mt-5">
                            <button type="submit" class="btn btn-danger mr-2">Ya, Hapus</button>
                            <a href="#" class="btn btn-light btn-dim" data-bs-dismiss="modal">Batal</a>
                        </div>
                    </div>
                </div>
            </form>
            
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
        </div>
    </div>
</div>