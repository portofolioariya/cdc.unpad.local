
<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="showData{{$jurusan->id}}">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <div class="mb-10">
                    <label for="name" class="required form-label">Fakultas</label>
                    <select class="form-select  @error('m_fakultas_id')
                    is-invalid
                @enderror" data-control="select2" data-placeholder="Select an option" name="m_fakultas_id" disabled>
                        <option></option>
                        @foreach ($fakultass as $fakultas)
                        <option value="{{$fakultas->id}}" @if (old('m_fakultas_id', $jurusan->m_fakultas->id) == $fakultas->id)
                            selected                                   
                        @endif>{{$fakultas->name}}</option>
                        @endforeach
                    </select>
                    @error('m_fakultas_id')
                    <div class="fv-plugins-message-container invalid-feedback">
                        <div data-field="login" data-validator="login">
                            {{ $message }}
                        </div>
                    </div>
                @enderror
                </div>
                <div class="form-group">
                    <label for="name" class="required form-label">Name</label>
                    <input type="text" name="name" class="form-control @error('name')
                        is-invalid
                    @enderror" placeholder="Please input name" value="{{old('name', $jurusan->name)}}" disabled />
                    @error('name')
                        <div class="fv-plugins-message-container invalid-feedback">
                            <div data-field="login" data-validator="login">
                                {{ $message }}
                            </div>
                        </div>
                    @enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>