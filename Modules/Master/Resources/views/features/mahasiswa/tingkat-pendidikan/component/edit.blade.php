<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="editData{{$gender->id}}">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
                <!--end::Close-->
            </div>
            <form action="{{URL('master-user-mahasiswa-tingkat-pendidikan/'.\Illuminate\Support\Facades\Crypt::encrypt($gender->id))}}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" name="name" class="form-control @error('name')
                            is-invalid
                        @enderror" placeholder="Please input name" value="{{old('name', $gender->name)}}" />
                        @error('name')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="login" data-validator="login">
                                    {{ $message }}
                                </div>
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>