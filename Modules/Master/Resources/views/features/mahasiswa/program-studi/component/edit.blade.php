<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" id="editData{{$prodi->id}}">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="close">X</span>
                </div>
                <!--end::Close-->
            </div>
            <form action="{{URL('master-user-mahasiswa-program-studi/'.\Illuminate\Support\Facades\Crypt::encrypt($prodi->id))}}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <div class="mb-10">
                            <label for="m_fakultas_id" class="required form-label">Fakultas</label>
                            <select id="form-edit-fakultas" class="form-select  @error('m_fakultas_id')
                            is-invalid
                            @enderror" name="m_fakultas_id">
                                    @foreach ($fakultass as $fakultas)
                                    <option value="{{$fakultas->id}}" @if (old('m_fakultas_id') == $fakultas->id)
                                        selected                                   
                                    @endif>{{$fakultas->name}}</option>
                                    @endforeach
                                </select>
                                @error('m_fakultas_id')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="m_fakultas_id" data-validator="m_fakultas_id">
                                        {{ $message }}
                                    </div>
                                </div>
                            @enderror
                        </div>

                        <div class="mb-10">
                            <label for="m_jurusan_id" class="required form-label">Jurusan</label>
                            <select id="form-edit-jurusan" class="form-select  @error('m_jurusan_id')
                            is-invalid
                            @enderror" name="m_jurusan_id">
                                    <option></option>
                                    {{-- @foreach ($jurusans as $jurusan)
                                    <option value="{{$jurusan->id}}" @if (old('m_jurusan_id') == $jurusan->id)
                                        selected                                   
                                    @endif>{{$jurusan->name}}</option>
                                    @endforeach --}}
                                </select>
                                @error('m_jurusan_id')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="m_jurusan_id" data-validator="m_jurusan_id">
                                        {{ $message }}
                                    </div>
                                </div>
                            @enderror
                        </div>

                        <div class="mb-10">
                            <label for="name" class="required form-label">Name</label>
                            <input type="text" name="name" class="form-control @error('name')
                                is-invalid
                            @enderror" placeholder="Please input name" value="{{old('name', $prodi->name)}}" />
                            @error('name')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="name" data-validator="name">
                                        {{ $message }}
                                    </div>
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>