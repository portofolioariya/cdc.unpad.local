<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<script>
    $(document).ready(function () {
        var fakultas_id = $("#form-create-fakultas").val();
        $.get("{{URL('master-user-mahasiswa-program-studi/getJurusan?id=')}}"+fakultas_id, 
            function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    $.each(data, function (index, jurusan) {
                        $('#form-create-jurusan').append('<option value="' + jurusan.id + '">' + jurusan.name + '</option>');
                    })
                } 
            },
            "json"
        );
    });

    $("#form-create-fakultas").on("change", function (e) {
        var fakultas_id = $("#form-create-fakultas").val();
        $.get("{{URL('master-user-mahasiswa-program-studi/getJurusan?id=')}}"+fakultas_id, 
            function (data, textStatus, jqXHR) {
                $('#form-create-jurusan').empty();
                $('#form-create-jurusan').append('<option selected disabled>Choose One</option>');
                if (jqXHR.status == 200) {
                    $.each(data, function (index, jurusan) {
                        $('#form-create-jurusan').append('<option value="' + jurusan.id + '">' + jurusan.name + '</option>');
                    })
                } 
            },
            "json"
        );
    });


    $(document).ready(function () {
        var fakultas_id = $("#form-edit-fakultas").val();
        $.get("{{URL('master-user-mahasiswa-program-studi/getJurusan?id=')}}"+fakultas_id, 
            function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    $.each(data, function (index, jurusan) {
                        $('#form-edit-jurusan').append('<option value="' + jurusan.id + '">' + jurusan.name + '</option>');
                    })
                } 
            },
            "json"
        );
    });

    $("#form-edit-fakultas").on("change", function (e) {
        var fakultas_id = $("#form-edit-fakultas").val();
        $.get("{{URL('master-user-mahasiswa-program-studi/getJurusan?id=')}}"+fakultas_id, 
            function (data, textStatus, jqXHR) {
                $('#form-edit-jurusan').empty();
                $('#form-edit-jurusan').append('<option selected disabled>Choose One</option>');
                if (jqXHR.status == 200) {
                    $.each(data, function (index, jurusan) {
                        $('#form-edit-jurusan').append('<option value="' + jurusan.id + '">' + jurusan.name + '</option>');
                    })
                } 
            },
            "json"
        );
    });

</script>