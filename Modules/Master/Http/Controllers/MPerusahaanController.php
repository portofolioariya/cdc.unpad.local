<?php

namespace Modules\Master\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Modules\Master\Entities\MCity;
use Modules\Master\Entities\MJabatan;
use App\Services\LogActivity\LogActivity;
use Modules\Master\Entities\MJenisPerusahaan;
use Modules\Master\Entities\MPerusahaan;

class MPerusahaanController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'master-user-mitra unpad';
    private $permissions = 'permission:master-user-mitra unpad-perusahaan';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            $jenisPerusahaan = MJenisPerusahaan::all();
            $city = MCity::all();
            $perusahaan = MPerusahaan::orderBy('created_at', 'desc')->get();
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('master::features.mitra-unpad.perusahaan.content.index', compact('perusahaan','jenisPerusahaan','city'));
        } catch (\Throwable $th) {
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        return view('master::create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => ['required'],
            'image'     => 'image|mimes:jpeg,png,jpg|max:2048',
            'city'       => ['required'],
            'jenis_perusahaan'        => ['required'],
            'email'       => ['required'],
            'no_tlp'        => ['required','numeric'],
        ]);

        DB::beginTransaction();
        try {
            $file = $request->file('image');
            $name = 'MP-'. Carbon::now()->format('Y-m-d-H-i-s') . '.' .$file->getClientOriginalExtension();
            $path = $file->storeAs('admin/Master/Perusahaan', '' . $name . '', 'public');

            MPerusahaan::create([
                'm_jenis_perusahaan_id'  => $request->jenis_perusahaan,
                'm_city_id'              => $request->city,
                'images'                  => $path,
                'name'                   => $request->name,
                'email'                  => $request->email,
                'no_tlp'                 => $request->no_tlp,
                'description'            => $request->desc,
                'created_by'             => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-perusahaan');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-perusahaan');
        }
    }

    public function show($id)
    {
        return view('master::show');
    }

    public function edit($id)
    {
        return view('master::edit');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'      => ['required'],
            'image'     => 'image|mimes:jpeg,png,jpg|max:2048',
            'city'       => ['required'],
            'jenis_perusahaan'        => ['required'],
            'email'       => ['required'],
            'no_tlp'        => ['required','numeric'],
        ]);

        $id = Crypt::decrypt($id);

        DB::beginTransaction();
        try {
            $perusahaan = MPerusahaan::find($id);

            if($request->image != null){
                $file = $request->file('image');
                $name = 'MP-'. Carbon::now()->format('Y-m-d-H-i-s') . '.' .$file->getClientOriginalExtension();
                $path = $file->storeAs('admin/Master/Perusahaan', '' . $name . '', 'public');

                File::delete('storage/'. $perusahaan->images);

            }else{
                $path = $perusahaan->images;
            }

            $perusahaan->update([
                'm_jenis_perusahaan_id'  => $request->jenis_perusahaan,
                'm_city_id'              => $request->city,
                'images'                  => $path,
                'name'                   => $request->name,
                'email'                  => $request->email,
                'no_tlp'                 => $request->no_tlp,
                'description'            => $request->desc,
                'updated_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-perusahaan');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-perusahaan');
        }
    }

    public function destroy($id)
    {
        $id = Crypt::decrypt($id);

        DB::beginTransaction();
        try {

            $perusahaan = MPerusahaan::find($id);
            $perusahaan->update([
                'deleted_by'  => Auth::user()->id,
            ]);
            $perusahaan->delete();
            File::delete('storage/'. $perusahaan->images);

            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-perusahaan');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-perusahaan');
        }
    }
}
