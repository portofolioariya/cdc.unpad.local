<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Master\Entities\MJabatan;
use App\Services\LogActivity\LogActivity;

class MJabatanController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'master-user-mitra unpad';
    private $permissions = 'permission:master-user-mitra unpad-jabatan';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            $jabatans = MJabatan::orderBy('created_at', 'desc')->paginate(10);
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('master::features.mitra-unpad.jabatan.content.index', compact('jabatans'));
        } catch (\Throwable $th) {
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        return view('master::create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  => ['required'],
        ]);

        DB::beginTransaction();
        try {
            MJabatan::create([
                'name'          => $request->name,
                'created_by'    => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-jabatan');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-jabatan');
        }
    }

    public function show($id)
    {
        return view('master::show');
    }

    public function edit($id)
    {
        return view('master::edit');
    }

    public function update(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $jabatan = MJabatan::find($id);

        $this->validate($request,[
            'name'  => ['required','unique:m_jabatans,name,'.$jabatan->name.',name',],
        ]);

        DB::beginTransaction();
        try {
            $jabatan->update([
                'name'  => $request->name,
                'updated_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-jabatan');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-jabatan');
        }
    }

    public function destroy($id)
    {
        $id = Crypt::decrypt($id);

        DB::beginTransaction();
        try {

            $jabatan = MJabatan::find($id);
            $jabatan->update([
                'deleted_by'  => Auth::user()->id,
            ]);

            $jabatan->delete();

            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/master-user-mitra-unpad-jabatan');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            Db::rollBack();
            return redirect('/master-user-mitra-unpad-jabatan');
        }
    }
}
