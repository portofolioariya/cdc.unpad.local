<?php

namespace Modules\Master\Http\Controllers;

use App\Services\LogActivity\LogActivity;
use App\Traits\Controller\GetData\getDataMaster;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Master\Entities\MFakultas;
use Modules\Master\Entities\MJurusan;
use Modules\Master\Entities\MProgramStudi;

class MProgramStudiController extends Controller
{
    use getDataMaster;

    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'master-user-mahasiswa-program studi';
    private $permissions = 'permission:master-user-mahasiswa-program studi';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            # code...
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            //code...
            $fakultass = MFakultas::all();
            $jurusans = MJurusan::all();
            $prodis = MProgramStudi::orderBy('created_at', 'desc')->paginate(10);
            // foreach ($prodis as $prodi) {
            //     # code...
            //     dd($prodi->m_jurusan->name);
            // }
          
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('master::features.mahasiswa.program-studi.content.index', compact('jurusans', 'fakultass', 'prodis'));
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        return view('master::create');
    }

    public function store(Request $request)
    {
        //
        // dd($request->all());
        $this->validate($request,[
            'm_jurusan_id' => ['required'],
            'name'  => ['required', 'unique:m_program_studis,name'],
        ]);

        DB::beginTransaction();
        try {
            //code...
            MProgramStudi::create([
                'm_jurusan_id' => $request->m_jurusan_id,
                'name'  => $request->name,
                'created_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/master-user-mahasiswa-program-studi/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/master-user-mahasiswa-program-studi/');
        }
    }

    public function show($id)
    {
        return view('master::show');
    }

    public function edit($id)
    {
        return view('master::edit');
    }

    public function update(Request $request, $id)
    {
        //
        $id = Crypt::decrypt($id);
        $gender = MProgramStudi::find($id);

        $this->validate($request,[
            'm_jurusan_id' => ['required'],
            'name'  => ['required','unique:m_program_studis,name,'.$gender->name.',name',],
        ]);
        DB::beginTransaction();
        try {
            //code...
            $gender->update([
                'm_jurusan_id' => $request->m_jurusan_id,
                'name'  => $request->name,
                'updated_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/master-user-mahasiswa-program-studi/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/master-user-mahasiswa-program-studi/');
        }
    }

    public function destroy($id)
    {
        //
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            //code...
            $gender = MProgramStudi::find($id);
            $gender->update([
                'deleted_by'  => Auth::user()->id,
            ]);
            $gender->delete();
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/master-user-mahasiswa-program-studi/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            DB::rollBack();
            return redirect('/master-user-mahasiswa-program-studi/');
        }
    }
}
