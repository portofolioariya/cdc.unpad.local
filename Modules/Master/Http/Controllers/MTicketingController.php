<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LogActivity\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Master\Entities\MGender;
use Modules\Master\Entities\MKategori;

class MTicketingController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'master-ticketing';
    private $permissions = 'permission:master-ticketing-kategori';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            # code...
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            $kategoris = MKategori::orderBy('created_at', 'desc')->paginate(10);
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('master::features.ticketing.content.index', compact('kategoris'));
        } catch (\Throwable $th) {
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        return view('master::create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  => ['required', 'unique:m_genders,name'],
        ]);

        DB::beginTransaction();
        try {
            MKategori::create([
                'name'  => $request->name,
                'created_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/master-ticketing-kategori');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/master-ticketing-kategori');
        }
    }

    public function show($id)
    {
        return view('master::show');
    }

    public function edit($id)
    {
        return view('master::edit');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'  => ['required', 'unique:m_genders,name'],
        ]);
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            $kategori = MKategori::find($id);
            $kategori->update([
                'name'  => $request->name,
                'updated_by'  => Auth::user()->id,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/master-ticketing-kategori');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/master-ticketing-kategori');
        }
    }

    public function destroy($id)
    {
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            $kategori = MKategori::find($id);
            $kategori->update([
                'deleted_by'  => Auth::user()->id,
            ]);
            $kategori->delete();
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/master-ticketing-kategori');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            Db::rollBack();
            return redirect('/master-ticketing-kategori');
        }
    }
}
