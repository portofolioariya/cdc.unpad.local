<?php

namespace Modules\Master\Entities;

use App\Models\UserProfile;
use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class MCity extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_province_id',
        'name',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MCityFactory::new();
    }

    public function m_province()
    {
        return $this->belongsTo(MProvince::class, 'm_province_id', 'id');
    }

    public function user_profile()
    {
        return $this->hasMany(UserProfile::class, 'm_city_id', 'id');
    }
}
