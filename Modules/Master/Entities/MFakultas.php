<?php

namespace Modules\Master\Entities;

use App\Models\UserEducation;
use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class MFakultas extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'name',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MFakultasFactory::new();
    }

    public function m_jurusans()
    {
        return $this->hasMany(MJurusan::class, 'm_jurusan_id', 'id');
    }

    public function user_education()
    {
        return $this->hasMany(UserEducation::class, 'm_fakultas_id', 'id');
    }
}
