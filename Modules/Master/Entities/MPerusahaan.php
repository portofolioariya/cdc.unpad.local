<?php

namespace Modules\Master\Entities;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Ticketing\Entities\TTicketing;

class MPerusahaan extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_jenis_perusahaan_id',
        'm_city_id',
        'images',
        'name',
        'email',
        'no_tlp',
        'description',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MJenisPerusahaanFactory::new();
    }

    public function m_jenis_perusahaan()
    {
        return $this->belongsTo(MJenisPerusahaan::class,'m_jenis_perusahaan_id','id');
    }

    public function m_city()
    {
        return $this->belongsTo(MCity::class,'m_city_id','id');
    }

    public function t_ticketing()
    {
        return $this->hasOne(TTicketing::class,'m_perusahaan_id','id');
    }

    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }
}
