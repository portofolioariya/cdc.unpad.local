<?php

namespace Modules\Master\Entities;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Ticketing\Entities\TTicketing;

class MKategori extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'name',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MKategoriFactory::new();
    }

    public function t_ticketing()
    {
        return $this->hasOne(TTicketing::class,'m_kategori_id','id');
    }
}
