<?php

namespace Modules\Master\Entities;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class MProvince extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'name',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MProvinceFactory::new();
    }

    public function m_cities()
    {
        return $this->hasMany(MCity::class, 'm_province_id', 'id');
    }

    public function user_profile()
    {
        return $this->hasMany(UserProfile::class, 'm_province_id', 'id');
    }
}
