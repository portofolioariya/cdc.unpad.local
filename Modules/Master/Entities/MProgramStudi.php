<?php

namespace Modules\Master\Entities;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class MProgramStudi extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_jurusan_id',
        'name',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Master\Database\factories\MProgramStudiFactory::new();
    }

    public function m_jurusan()
    {
        return $this->belongsTo(MJurusan::class, 'm_jurusan_id', 'id');
    }

    public function user_education()
    {
        return $this->hasMany(UserEducation::class, 'm_program_studi_id', 'id');
    }
}
