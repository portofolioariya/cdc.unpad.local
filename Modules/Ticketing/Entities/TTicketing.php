<?php

namespace Modules\Ticketing\Entities;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\MKategori;
use Modules\Master\Entities\MPerusahaan;

class TTicketing extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_kategori_id',
        'm_perusahaan_id',
        'images',
        'name',
        'title',
        'description',
        'from',
        'to',
        'feature_code',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    protected static function newFactory()
    {
        return \Modules\Ticketing\Database\factories\TTicketingFactory::new();
    }

    public function m_perusahaan()
    {
        return $this->belongsTo(MPerusahaan::class,'m_perusahaan_id','id');
    }

    public function m_kategori()
    {
        return $this->belongsTo(MKategori::class,'m_kategori_id','id');
    }
}
