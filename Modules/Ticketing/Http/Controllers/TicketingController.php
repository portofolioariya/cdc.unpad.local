<?php

namespace Modules\Ticketing\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Ticketing\Entities\TTicketing;

class TicketingController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'Feature-ticketing';
    private $permissions = 'permission:ticketing';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        $ticket = TTicketing::orderBy('created_at', 'desc')->get();
        return view('ticketing::features.ticketing.content.index', compact('ticket'));
    }

    public function create()
    {
        return view('ticketing::create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('ticketing::show');
    }

    public function edit($id)
    {
        return view('ticketing::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
