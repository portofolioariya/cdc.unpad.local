<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use \Modules\Ticketing\Http\Controllers\TicketingController;

Route::group(['middleware' => 'auth'], function (){

    Route::group(['middleware' => ['permission:Feature-ticketing']], function (){
        Route::prefix('ticketing')->group(function() {
            Route::group(['middleware' => ['permission:ticketing-index']], function (){
                Route::get('/', [TicketingController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:ticketing-show']], function (){
                Route::post('/{id}', [TicketingController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:ticketing-edit']], function (){
                Route::put('/{id}', [TicketingController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:ticketing-update']], function (){
                Route::put('/{id}', [TicketingController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:ticketing-delete']], function (){
                Route::delete('/{id}', [TicketingController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:ticketing-approve']], function (){
                Route::put('/{id}/approve', [TicketingController::class, 'approve']);
            });
        });
    });

});
