<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_ticketings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_kategori_id')->unsigned();
            $table->foreign('m_kategori_id')->references('id')->on('m_kategoris')->onDelete('cascade');
            $table->bigInteger('m_perusahaan_id')->unsigned();
            $table->foreign('m_perusahaan_id')->references('id')->on('m_perusahaans')->onDelete('cascade');
            $table->bigInteger('ticket_code');
            $table->string('images');
            $table->string('title');
            $table->string('description');
            $table->string('from');
            $table->string('to');
            $table->bigInteger('feature_code')->nullable();
            $table->enum('approved_status',['confirmed', 'unconfirmed', 'pending'])->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_ticketings');
    }
}
