<div class="d-flex flex-wrap flex-stack my-5">
    <h2 class="fs-2 fw-bold my-2">Projects
    <span class="fs-6 text-gray-400 ms-1">by Status</span></h2>
    <div class="d-flex align-items-center py-3 py-md-1" data-select2-id="select2-data-125-yree">
        <div class="me-4" data-select2-id="select2-data-124-8u1n">
            <a href="#" class="btn btn-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                <span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black"></path>
                    </svg>
                </span>
                Filter
            </a>
            <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61bf2f928ee03" style="" data-select2-id="select2-data-kt_menu_61bf2f928ee03">
                <div class="px-7 py-5">
                    <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                </div>
                <div class="separator border-gray-200"></div>
                <div class="px-7 py-5" data-select2-id="select2-data-123-du6m">
                    <div class="mb-10" data-select2-id="select2-data-122-rhp1">
                        <label class="form-label fw-bold">Status:</label>
                        <div>
                            <select class="form-select form-select-solid select2-hidden-accessible" data-kt-select2="true" data-placeholder="Select option" data-dropdown-parent="#kt_menu_61bf2f928ee03" data-allow-clear="true" data-select2-id="select2-data-7-kt5g" tabindex="-1" aria-hidden="true">
                                <option data-select2-id="select2-data-9-alss"></option>
                                <option value="1" data-select2-id="select2-data-127-spxb">Approved</option>
                                <option value="2" data-select2-id="select2-data-128-he12">Pending</option>
                                <option value="2" data-select2-id="select2-data-129-el8a">In Process</option>
                                <option value="2" data-select2-id="select2-data-130-slls">Rejected</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
