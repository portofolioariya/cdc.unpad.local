<div class="col-md-6 col-xl-4">
    <a href="#" class="card border-hover-primary">
        <div class="card-header border-0 pt-9">
            <div class="card-title m-0">
                <div class="symbol symbol-50px w-50px bg-light">
                    <img src="{{('assets/dist/assets/media/svg/brand-logos/plurk.svg')}}" alt="image" class="p-3">
                </div>
            </div>
            <div class="card-toolbar">
                <div class="badge badge-light-danger fw-bolder px-4 py-3" style="margin-right: 10px">Open</div>
            </div>
        </div>
        <div class="card-body p-9">
            <div class="fs-3 fw-bolder text-dark">Nama Perusahaan</div>
            <p class="text-gray-400 fw-bold fs-5 mt-1 mb-7">Jenis Perusahaan</p>
            <div class="d-flex flex-wrap mb-5">
                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                    <div class="fs-6 text-gray-800 fw-bolder">5</div>
                    <div class="fw-bold text-gray-400">Jumlah Anggota</div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <span class="badge badge-light-info">Email</span>
        </div>
    </a>
</div>
