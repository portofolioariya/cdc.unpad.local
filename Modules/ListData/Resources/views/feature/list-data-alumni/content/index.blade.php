@extends('layouts.base-layout.content.base-layout')
@push('title', 'Test')
@section('content')
<div class="">
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <!--begin::Post-->
        <div class="content flex-row-fluid" id="kt_content">
            @include('listdata::feature.list-data-alumni.component.header')
            <div class="row g-6 g-xl-9">
               @include('listdata::feature.list-data-alumni.component.card')
            </div>
        </div>
    </div>
</div>
@endsection
