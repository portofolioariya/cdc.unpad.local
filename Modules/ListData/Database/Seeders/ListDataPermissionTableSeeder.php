<?php

namespace Modules\ListData\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class ListDataPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $permissions = [
            ['name' => 'list-mahasiswa'],
                ['name' => 'list-mahasiswa-index'],
                ['name' => 'list-mahasiswa-create'],
                ['name' => 'list-mahasiswa-store'],
                ['name' => 'list-mahasiswa-show'],
                ['name' => 'list-mahasiswa-edit'],
                ['name' => 'list-mahasiswa-update'],
                ['name' => 'list-mahasiswa-delete'],
                ['name' => 'list-mahasiswa-approve'],
            ['name' => 'list-perusahaan'],
                ['name' => 'list-perusahaan-index'],
                ['name' => 'list-perusahaan-create'],
                ['name' => 'list-perusahaan-store'],
                ['name' => 'list-perusahaan-show'],
                ['name' => 'list-perusahaan-edit'],
                ['name' => 'list-perusahaan-update'],
                ['name' => 'list-perusahaan-delete'],
                ['name' => 'list-perusahaan-approve'],
        ];

        foreach ($permissions as $permission) {
            # code...
            Permission::create($permission);
        }
        // $this->call("OthersTableSeeder");
    }
}
