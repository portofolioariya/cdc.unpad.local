<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Modules\Rbac\Http\Controllers\PermissionController;
use Modules\Rbac\Http\Controllers\RoleController;
use Modules\Rbac\Http\Controllers\UserController;

Route::group(['middleware' => 'auth'], function (){
    Route::group(['middleware' => ['permission:rbac-permission']], function (){
        Route::prefix('rbac-permission')->group(function() {
            Route::group(['middleware' => ['permission:rbac-permission-index']], function (){
                Route::get('/', [PermissionController::class, 'index']);
            });
        });
    });

    Route::group(['middleware' => ['permission:rbac-role']], function (){
        Route::prefix('rbac-role')->group(function() {
            Route::group(['middleware' => ['permission:rbac-role-index']], function (){
                Route::get('/', [RoleController::class, 'index']);
            });
            Route::group(['middleware' => ['permission:rbac-role-store']], function (){
                Route::post('/', [RoleController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:rbac-role-update']], function (){
                Route::put('/{id}', [RoleController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:rbac-role-delete']], function (){
                Route::delete('/{id}', [RoleController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:rbac-role-approve']], function (){
                Route::put('/{id}/approve', [RoleController::class, 'approve']);
            });
        });
    });

    Route::group(['middleware' => ['permission:rbac-user']], function (){
        Route::prefix('rbac-user')->group(function() {
            Route::group(['middleware' => ['permission:rbac-user-index']], function (){
                Route::get('/', [UserController::class, 'index']);
            });

            Route::group(['middleware' => ['permission:rbac-user-create']], function (){
                Route::get('/create', [UserController::class, 'create']);
            });

            Route::group(['middleware' => ['permission:rbac-user-show']], function (){
                Route::get('/{id}', [UserController::class, 'show']);
            });

            Route::group(['middleware' => ['permission:rbac-user-edit']], function (){
                Route::get('/{id}/edit', [UserController::class, 'edit']);
            });

            Route::group(['middleware' => ['permission:rbac-user-store']], function (){
                Route::post('/', [UserController::class, 'store']);
            });
            Route::group(['middleware' => ['permission:rbac-user-update']], function (){
                Route::put('/{id}', [UserController::class, 'update']);
            });
            Route::group(['middleware' => ['permission:rbac-user-delete']], function (){
                Route::delete('/{id}', [UserController::class, 'destroy']);
            });
            Route::group(['middleware' => ['permission:rbac-user-approve']], function (){
                Route::put('/{id}/approve', [UserController::class, 'approve']);
            });
        });
    });
});
