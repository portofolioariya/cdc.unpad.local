<?php

namespace Modules\Rbac\Http\Controllers;

use App\Services\LogActivity\LogActivity;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'rbac-permission';
    private $permissions = 'permission:rbac-permission';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            # code...
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            //code...
            $genders = Permission::orderBy('created_at', 'desc')->get();
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('rbac::features.permission.content.index', compact('genders'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }
}
