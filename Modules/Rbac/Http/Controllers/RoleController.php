<?php

namespace Modules\Rbac\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LogActivity\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Master\Entities\MGender;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'rbac-role';
    private $permissions = 'permission:rbac-role';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            # code...
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            //code...
            $permissions = Permission::all();
            $roles = Role::orderBy('created_at', 'desc')->get();
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('rbac::features.role.content.index', compact('roles', 'permissions'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        return view('master::create');
    }

    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'  => ['required', 'unique:roles,name'],
            'permission'  => ['required',],
        ]);

        DB::beginTransaction();
        try {
        //     //code...
            $role = Role::create([
                'name'  => $request->name,
                // 'created_by'  => Auth::user()->id,
            ]);
            $permission = Permission::whereIn('id', $request->permission)->get();
            $role->syncPermissions($permission);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/rbac-role/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/rbac-role/');
        }
    }

    public function show($id)
    {
        return view('master::show');
    }

    public function edit($id)
    {
        return view('master::edit');
    }

    public function update(Request $request, $id)
    {
        //
        $id = Crypt::decrypt($id);
        $role = Role::find($id);
        $this->validate($request,[
            'name'  => ['required', 'unique:roles,name,'.$role->name.',name',],
            'permission'  => ['required',],
        ]);
        DB::beginTransaction();
        try {
            //code...
            $role->update([
                'name'  => $request->name,
                // 'updated_by'  => Auth::user()->id,
            ]);
            $permission = Permission::whereIn('id', $request->permission)->get();
            $role->syncPermissions($permission);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/rbac-role/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/rbac-role/');
        }
    }

    public function destroy($id)
    {
        //
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            //code...
            $role = Role::find($id);
            $role->update([
                'deleted_by'  => Auth::user()->id,
            ]);
            $role->delete();
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/rbac-role/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            Db::rollBack();
            return redirect('/rbac-role/');
        }
    }
}
