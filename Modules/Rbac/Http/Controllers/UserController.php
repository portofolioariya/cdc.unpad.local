<?php

namespace Modules\Rbac\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\LogActivity\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Modules\Master\Entities\MJabatan;
use Modules\Master\Entities\MPerusahaan;
use Modules\Master\Entities\MUserType;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    private $access_permissions = ['index','create', 'store', 'show', 'edit', 'update', 'delete', 'approve'];
    private $base_permission = 'rbac-user';
    private $permissions = 'permission:rbac-user';

    public function __construct()
    {
        foreach ($this->access_permissions as $access) {
            # code...
            $this->permissions =  $this->permissions. '|' . $this->base_permission. '-' . $access;
        }
        $this->middleware([$this->permissions]);
    }

    public function index()
    {
        DB::beginTransaction();
        try {
            //code...
            $users = User::all();
            LogActivity::addToLog($this->permissions, 'index', 'success');
            DB::commit();
            return view('rbac::features.user.content.index', compact('users'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function create()
    {
        DB::beginTransaction();
        try {
            //code...
            $user_type = MUserType::all();
            $roles = Role::all();
            $perusahaan = MPerusahaan::all();
            $jabatan = MJabatan::all();
            LogActivity::addToLog($this->permissions, 'create', 'success');
            DB::commit();
            return view('rbac::features.user.content.create', compact('user_type', 'roles','perusahaan', 'jabatan',));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'create', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function store(Request $request)
    {
        //
        // dd($request->all());
        $this->validate($request,[
            'user_type' => ['required'],
            'roles' => ['required'],
            'first_name' => ['required', 'string','max:255'],
            'last_name' => ['required', 'string','max:255'],
            'username' => ['required', 'string','max:255', 'unique:users,username'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        DB::beginTransaction();
        try {
            //code...
        if ($request->user_type == '1') {
                $user = User::create([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->attach($user_type->id);
                $user->assignRole($request->roles);

                $user->profile()->create([
                    'm_province_id' => null,
                    'm_city_id' => null,
                    'm_gender_id' => null,
                    'images' => null,
                    'no_telp' => null,
                    'tgl_lahir' => null,
                    'address' => null,
                    'status_pekerjaan' => null,
                    'status_pernikahan' => null,
                    'about_me' => null,
                    'created_by' => Auth::user()->id
                ]);

                $user->education()->create([
                    'm_tingkat_pendidikan_id' => null,
                    'm_fakultas_id' => null,
                    'm_jurusan_id' => null,
                    'm_program_studi_id' => null,
                    'nim' => $request->nim,
                    'year_start' => null,
                    'year_end' => null,
                    'tema_skripsi' => null,
                    'judul_skripsi' => null,
                    'deskripsi_skripsi' => null,
                    'ipk' => null,
                    'created_by' => Auth::user()->id
                ]);
            }

            if ($request->user_type == '2') {
                $user = User::create([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->attach($user_type->id);
                $user->jabatan()->attach($request->jabatan);
                $user->perusahaan()->attach($request->perusahaan);
                $user->assignRole($request->roles);

                $user->profile()->create([
                    'm_province_id' => null,
                    'm_city_id' => null,
                    'm_gender_id' => null,
                    'images' => null,
                    'no_telp' => null,
                    'tgl_lahir' => null,
                    'address' => null,
                    'status_pekerjaan' => null,
                    'status_pernikahan' => null,
                    'about_me' => null,
                    'created_by' => Auth::user()->id
                ]);
            }

            if ($request->user_type == '3') {
                $user = User::create([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->attach($user_type->id);
                $user->assignRole($request->roles);

                $user->profile()->create([
                    'm_province_id' => null,
                    'm_city_id' => null,
                    'm_gender_id' => null,
                    'images' => null,
                    'no_telp' => null,
                    'tgl_lahir' => null,
                    'address' => null,
                    'status_pekerjaan' => null,
                    'status_pernikahan' => null,
                    'about_me' => null,
                    'created_by' => Auth::user()->id
                ]);
            }

            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Create data success!');
            LogActivity::addToLog($this->permissions, 'store', 'success');
            DB::commit();
            return redirect('/rbac-user/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Create data fail!');
            LogActivity::addToLog($this->permissions, 'store', 'fail');
            Db::rollBack();
            return redirect('/rbac-user/');
        }
    }

    public function show($id)
    {
        // dd($id);
        DB::beginTransaction();
        try {
            //code...
            $id = Crypt::decrypt($id);
            $user = User::find($id);
            $user_type = MUserType::all();
            $roles = Role::all();
            $perusahaan = MPerusahaan::all();
            $jabatan = MJabatan::all();
            LogActivity::addToLog($this->permissions, 'show', 'success');
            DB::commit();
            return view('rbac::features.user.content.show', compact('user','user_type', 'user', 'roles', 'jabatan', 'perusahaan'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'show', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function edit($id)
    {
        $id = Crypt::decrypt($id);
            $user = User::find($id);
            // dd($user->roles()->first()->id);
        DB::beginTransaction();
        try {
            //code...
            $user_type = MUserType::all();
            $roles = Role::all();
            $perusahaan = MPerusahaan::all();
            $jabatan = MJabatan::all();
            LogActivity::addToLog($this->permissions, 'edit', 'success');
            DB::commit();
            return view('rbac::features.user.content.edit  ', compact('user_type', 'user', 'roles', 'jabatan', 'perusahaan'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog($this->permissions, 'edit', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function update(Request $request, $id)
    {
        //
        $id = Crypt::decrypt($id);
        $user = User::find($id);

        $this->validate($request,[
            'user_type' => ['required'],
            'roles' => ['required'],
            'first_name' => ['required', 'string','max:255'],
            'last_name' => ['required', 'string','max:255'],
            'username'  => ['required', 'unique:users,username,'.$user->username.',username',],
            'email'  => ['required', 'unique:users,email,'.$user->email.',email',],

            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        DB::beginTransaction();
        try {
            //code...
            if ($request->user_type == 1) {
                # code...
                $user->update([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->sync([$user_type->id]);
                $user->syncRoles($request->roles);
                $user->education()->first()->update([
                    'nim' => $request->nim,
                ]);

            }
            if ($request->user_type == 2) {
                # code...
                $user->update([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->sync([$user_type->id]);
                $user->jabatan()->sync($request->jabatan);
                $user->perusahaan()->sync($request->perusahaan);
                $user->syncRoles($request->roles);
            }

            if ($request->user_type == '3') {
                $user->update([
                    'first_name'      => $request->first_name,
                    'last_name'      => $request->last_name,
                    'username'  => $request->username,
                    'email'     => $request->email,
                    'password'  => Hash::make($request->password),
                ]);

                $user_type = MUserType::find($request->user_type);
                $user->user_type()->sync($user_type->id);
                $user->syncRoles($request->roles);
            }
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Update data success!');
            LogActivity::addToLog($this->permissions, 'update', 'success');
            DB::commit();
            return redirect('/rbac-user/');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Update data fail!');
            LogActivity::addToLog($this->permissions, 'update', 'fail');
            Db::rollBack();
            return redirect('/rbac-user/');
        }
    }

    public function destroy($id)
    {
        //
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            //code...
            $user = User::find($id);
            $user->update([
                'deleted_by'  => Auth::user()->id,
            ]);
            $user->delete();
            Session::flash('status', 'success');
            Session::flash('messages', 'Success');
            Session::flash('info', 'Delete data success!');
            LogActivity::addToLog($this->permissions, 'destroy', 'success');
            DB::commit();
            return redirect('/rbac-user/');
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail!');
            Session::flash('info', 'Delete data fail!');
            LogActivity::addToLog($this->permissions, 'destroy', 'fail');
            Db::rollBack();
            return redirect('/rbac-user/');
        }
    }
}
