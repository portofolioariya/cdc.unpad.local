@extends('layouts.base-layout.content.base-layout')
@push('title', 'Form Master Permission')
@section('content')
<div>
    <div class="card shadow-sm mt-5">
        <div class="card-header">
            <h3 class="card-title">Master Permission</h3>
            <div class="card-toolbar">
                {{-- @include('master::features.gender.component.create') --}}
            </div>
        </div>
        {{-- <div class="mb-10">
            <select class="form-select select_group" data-control="select2" data-placeholder="Select an option">
                <option></option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
            </select>
        </div> --}}
        <div class="card-body">
            <table id="example" class="table table-striped text-center" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($genders as $gender)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ucwords(str_replace("-", " ", $gender->name))}}</td>
                    </tr>
                    @endforeach
                </tbody>
                {{-- <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </tfoot> --}}
            </table>
            
        </div>
        {{-- <div class="card-footer">
            <button type="button" class="btn btn-sm btn-primary">
                Action
            </button>
        </div> --}}
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
