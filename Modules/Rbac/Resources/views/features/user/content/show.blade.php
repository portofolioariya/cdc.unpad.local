@extends('layouts.base-layout.content.base-layout')
@push('title', 'Form Rbac User')
@section('content')
<div>
    <div class="card shadow-sm mt-5">
        <div class="card-header">
            <h3 class="card-title">Rbac User</h3>
            <div class="card-toolbar">
            </div>
        </div>

        <div class="card-body d-flex justify-content-center">
            <div class="col-8">
                <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{URL('rbac-user/'.\Illuminate\Support\Facades\Crypt::encrypt($user->id))}}"
                    method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row mb-10">
                        <div class="mb-10">
                            <label for="name" class="required form-label">User Type</label>
                            <select disabled class="form-select  @error('user_type')
                            is-invalid @enderror" name="user_type" id="user_type">
                                @foreach ($user_type as $type)
                                <option value="{{$type->id}}" @if (old('user_type', $user->user_type()->first()->id)==$type->id)
                                    selected
                                    @endif>{{$type->name}}</option>
                                @endforeach
                            </select>
                            @error('user_type')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="user_type" data-validator="user_type">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="mb-10">
                            <label for="name" class="required form-label">Role</label>
                            <select disabled class="form-select  @error('roles')
                            is-invalid @enderror" name="roles" id="roles">
                                @foreach ($roles as $type)
                                <option value="{{$type->id}}" @if (old('roles',  $user->roles()->first()->id)==$type->id)
                                    selected
                                    @endif>{{$type->name}}</option>
                                @endforeach
                            </select>
                            @error('roles')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="roles" data-validator="roles">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="row fv-row mb-7" id="mitra-unpad-master">
                            <div class="col-xl-6">
                                <div class="mb-10">
                                    <label for="name" class=" form-label">Perusahaan</label>
                                    <select disabled class="form-select  @error('perusahaan')
                                    is-invalid @enderror" name="perusahaan" id="perusahaan">
                                        @foreach ($perusahaan as $type)
                                        <option value="{{$type->id}}" @if (old('perusahaan')==$type->id)
                                            selected
                                            @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('perusahaan')
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="perusahaan" data-validator="perusahaan">
                                            {{ $message }}
                                        </div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="mb-10">
                                    <label for="name" class=" form-label">Jabatan</label>
                                    <select disabled class="form-select  @error('jabatan')
                                    is-invalid @enderror" name="jabatan" id="jabatan">
                                        @foreach ($jabatan as $type)
                                        <option value="{{$type->id}}" @if (old('jabatan')==$type->id)
                                            selected
                                            @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('jabatan')
                                    <div class="fv-plugins-message-container invalid-feedback">
                                        <div data-field="jabatan" data-validator="jabatan">
                                            {{ $message }}
                                        </div>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row fv-row mb-7">
                            <div class="col-xl-6">
                                <label class="form-label fw-bolder text-dark fs-6">First Name @error('first_name') <i
                                        class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                                <input disabled class="form-control form-control-lg" type="text"
                                    placeholder="" name="first_name" value="{{old('first_name', $user->first_name)}}" autocomplete="off" />
                                @error('first_name')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="first_name" data-validator="first_name">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>
                            <div class="col-xl-6">
                                <label class="form-label fw-bolder text-dark fs-6">Last Name @error('last_name') <i
                                        class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                                <input disabled class="form-control form-control-lg" type="text"
                                    placeholder="" name="last_name" value="{{old('last_name', $user->last_name)}}" autocomplete="off" />
                                @error('last_name')
                                <div class="fv-plugins-message-container invalid-feedback">
                                    <div data-field="last_name" data-validator="last_name">
                                        {{ $message }}
                                    </div>
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="fv-row mb-7" id="temp-nim">
                            <label class="form-label fw-bolder text-dark fs-6">Nim @error('nim') <i
                                    class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                            <input disabled class="form-control form-control-lg" type="nim" placeholder=""
                                name="nim" value="{{old('nim')}}" autocomplete="off" />
                            @error('nim')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="nim" data-validator="nim">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fw-bolder text-dark fs-6">Username @error('username') <i
                                    class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                            <input disabled class="form-control form-control-lg" type="username"
                                placeholder="" name="username" value="{{old('username',$user->username)}}" autocomplete="off" />
                            @error('username')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="username" data-validator="username">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="fv-row mb-7">
                            <label class="form-label fw-bolder text-dark fs-6">Email @error('email') <i
                                    class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                            <input disabled class="form-control form-control-lg" type="email" placeholder=""
                                name="email" value="{{old('email',$user->email)}}" autocomplete="off" />
                            @error('email')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="email" data-validator="email">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="mb-10 fv-row" data-kt-password-meter="true">
                            <div class="mb-1">
                                <label class="form-label fw-bolder text-dark fs-6">Password @error('password') <i
                                        class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                                <div class="position-relative mb-3">
                                    <input disabled class="form-control form-control-lg" type="password"
                                        placeholder="" name="password" value="{{old('password')}}" autocomplete="off" />
                                    <span
                                        class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                                        data-kt-password-meter-control="visibility">
                                        <i class="bi bi-eye-slash fs-2"></i>
                                        <i class="bi bi-eye fs-2 d-none"></i>
                                    </span>
                                </div>
                                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                                </div>
                            </div>
                            <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp;
                                symbols.</div>
                            @error('password')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="password" data-validator="password">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror

                        </div>
                        <div class="fv-row mb-5">
                            <label class="form-label fw-bolder text-dark fs-6">Confirm Password
                                @error('password_confirmation') <i class="text-danger fas fa-exclamation-circle"></i>
                                @enderror </label>
                            <input disabled class="form-control form-control-lg" type="password"
                                placeholder="" name="password_confirmation" autocomplete="off" />
                            @error('password_confirmation')
                            <div class="fv-plugins-message-container invalid-feedback">
                                <div data-field="password_confirmation" data-validator="password_confirmation">
                                    {{ $message }}
                                </div>
                            </div>
                            @enderror
                        </div>
                        <div class="text-center row">
                            <div class="col-6">
                                <a type="submit" id="kt_sign_in_back" class="btn btn-lg btn-secondary w-100 mb-5"
                                    href="{{ url()->previous() }}">
                                    <span class="indicator-label">Back</span>
                                </a>
                            </div>

                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
    $(document).ready(function () {
        var user_type = $("#user_type").val();
            if (user_type == 2) {
                $('#temp-nim').hide();
                $('#mitra-unpad-master').show();
            } else if(user_type == 1) {
                $('#temp-nim').show();
                $('#mitra-unpad-master').hide();
            } else {
                $('#temp-nim').hide();
                $('#mitra-unpad-master').hide();
            }
        $("#user_type").on('change', function () {
            user_type = $("#user_type").val();
            if (user_type == 2) {
                $('#temp-nim').hide();
                $('#mitra-unpad-master').show();
            } else if(user_type == 1) {
                $('#temp-nim').show();
                $('#mitra-unpad-master').hide();
            } else {
                $('#temp-nim').hide();
                $('#mitra-unpad-master').hide();
            }
        });
    });

</script>
@endsection
