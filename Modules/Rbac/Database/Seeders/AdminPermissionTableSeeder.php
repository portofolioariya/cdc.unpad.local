<?php

namespace Modules\Rbac\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class AdminPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $permissions = [
            ['name' => 'rbac'],
                ['name' => 'rbac-user'],
                    ['name' => 'rbac-user-index'],
                    ['name' => 'rbac-user-create'],
                    ['name' => 'rbac-user-store'],
                    ['name' => 'rbac-user-show'],
                    ['name' => 'rbac-user-edit'],
                    ['name' => 'rbac-user-update'],
                    ['name' => 'rbac-user-delete'],
                    ['name' => 'rbac-user-approve'],
                    
                ['name' => 'rbac-role'],
                    ['name' => 'rbac-role-index'],
                    ['name' => 'rbac-role-create'],
                    ['name' => 'rbac-role-store'],
                    ['name' => 'rbac-role-show'],
                    ['name' => 'rbac-role-edit'],
                    ['name' => 'rbac-role-update'],
                    ['name' => 'rbac-role-delete'],
                    ['name' => 'rbac-role-approve'],

                ['name' => 'rbac-permission'],
                    ['name' => 'rbac-permission-index'],
                    ['name' => 'rbac-permission-create'],
                    ['name' => 'rbac-permission-store'],
                    ['name' => 'rbac-permission-show'],
                    ['name' => 'rbac-permission-edit'],
                    ['name' => 'rbac-permission-update'],
                    ['name' => 'rbac-permission-delete'],
                    ['name' => 'rbac-permission-approve'],
        ];

        foreach ($permissions as $permission) {
            # code...
            Permission::create($permission);
        }
        // $this->call("OthersTableSeeder");
    }
}
