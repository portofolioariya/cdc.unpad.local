<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Services\LogActivity\LogActivity;
use App\Services\Menu\BreadCumb;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Modules\ListData\Http\Controllers\ListDataAlumniController;
use Modules\ListData\Http\Controllers\ListDataController;
use Modules\ListData\Http\Controllers\ListDataPerusahaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::post('/register', [RegisterController::class, 'create']);




Auth::routes();
Route::group(['middleware' => 'auth'], function (){
    Route::get('/dashboard', function () {
        return view('layouts.base-layout.example.index');
    });
    Route::get('/profile/{id}', [ProfileController::class, 'index']);
    Route::get('/account-setting/{id}', [ProfileController::class, 'account_setting']);
});

Route::get('/dashboard', function () {
    return view('layouts.base-layout.example.index');
});

Route::group(['middleware' => 'auth'], function (){
        Route::get('/list-alumni', [ListDataAlumniController::class, 'index']);
});

Route::group(['middleware' => 'auth'], function (){
    Route::get('/list-perusahaan', [ListDataPerusahaanController::class, 'index']);
});

Route::get('/home', [HomeController::class, 'index'])->name('home');

