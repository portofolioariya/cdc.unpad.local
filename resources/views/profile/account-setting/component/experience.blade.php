
    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Experience</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="expGroup">
                        <div class="row">
                            <div class="col-xl-5">
                                    <div class="fv-row mb-10">
                                        <label class="required fw-bold fs-6 mb-2">Nama Perusahaan</label>
                                        <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                    </div>
                            </div>
                            <div class="col-xl-5">
                                <div class="fv-row mb-10">
                                    <label class="required fw-bold fs-6 mb-2">Jabatan</label>
                                    <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                        <option></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-5">
                                    <div class="fv-row mb-10">
                                        <label class="required fw-bold fs-6 mb-2">Tanggal Masuk</label>
                                        <input type="date" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                    </div>
                            </div>
                            <div class="col-xl-5 addDeleteExp">
                                <div class="fv-row mb-10">
                                    <label class="required fw-bold fs-6 mb-2">Tanggal Keluar</label>
                                    <input type="date" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                </div>
                            </div>
                            <div class="col-xl-2 deleteExp" style="display: none">
                                <div class="fv-row mb-10">
                                    <a id="removeExp" type="reset" class="btn btn-light-danger me-2" style="margin-top: 28px">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a class="btn btn-light me-2 addRowExperience">Add Row</a>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
