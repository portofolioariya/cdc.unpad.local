<div class="card mb-5 mb-xl-10">
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Contact</h3>
        </div>
    </div>
    <div id="kt_account_profile_details" class="collapse show">
        <form id="kt_account_profile_details_form" class="form">
            <div class="card-body border-top p-9">
                <div class="row contactGroup">
                    <div class="col-xl-5">
                        <div class="fv-row mb-10">
                            <label class="required fw-bold fs-6 mb-2">Social Media</label>
                            <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                        </div>
                    </div>
                    <div class="col-xl-5 addDeleteContact">
                        <div class="fv-row mb-10">
                            <label class="required fw-bold fs-6 mb-2">Account Link</label>
                            <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                        </div>
                    </div>
                    <div class="col-xl-2 deleteContact" style="display: none">
                        <div class="fv-row mb-10">
                            <a id="removeContact" type="reset" class="btn btn-light-danger me-2" style="margin-top: 28px">Delete</a>
                        </div>
                    </div>
                </div>
                <div>
                    <a class="btn btn-light me-2 addRowContact">Add Row</a>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
            </div>
        </form>
    </div>
</div>
