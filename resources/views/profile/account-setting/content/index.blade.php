@extends('layouts.base-layout.content.base-layout')
@push('title', 'Test')
@section('content')
<div class="">
    @include('profile.account-setting.component.overview')

    @include('profile.account-setting.component.profile-detils')

    @include('profile.account-setting.component.skill')

    @include('profile.account-setting.component.experience')

    @include('profile.account-setting.component.education')

    @include('profile.account-setting.component.contact')

    @include('profile.account-setting.component.perusahaan')


</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".addRowSkill").click(function () {
            var fieldHTML = '<div class="row skillGroup">' + $(".skillGroup").html();
            $('body').find('.skillGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteSkill">' + $(".deleteSkill").html() + '</div>';
            $('body').find('.addDeleteSkill:last').after(btnRemove);
        });
        $("body").on("click", "#removeSkill", function () {
            let currow = $(this).closest('.skillGroup');
            $(this).parents(".skillGroup").remove();
        });




        $(".addRowExperience").click(function () {
            var fieldHTML = '<div class="expGroup">' + $(".expGroup").html();
            $('body').find('.expGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteExp">' + $(".deleteExp").html() + '</div>';
            $('body').find('.addDeleteExp:last').after(btnRemove);
        });
        $("body").on("click", "#removeExp", function () {
            let currow = $(this).closest('.expGroup');
            $(this).parents(".expGroup").remove();
        });



        $(".addRowContact").click(function () {
            var fieldHTML = '<div class="row contactGroup">' + $(".contactGroup").html();
            $('body').find('.contactGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteContact">' + $(".deleteContact").html() + '</div>';
            $('body').find('.addDeleteContact:last').after(btnRemove);
        });
        $("body").on("click", "#removeContact", function () {
            let currow = $(this).closest('.contactGroup');
            $(this).parents(".contactGroup").remove();
        });
    });

</script>
@endsection
