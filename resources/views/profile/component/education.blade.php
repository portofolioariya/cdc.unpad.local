<div class="col-xl-6">
    <!--begin::List Widget 5-->
    <div class="card card-xl-stretch mb-xl-10">
        <!--begin::Header-->
        <div class="card-header align-items-center border-0 mt-4">
            <h3 class="card-title align-items-start flex-column">
                <span class="fw-bolder mb-2 text-dark">Educations</span>
                <span class="text-muted fw-bold fs-7">My education since 2005 - 2020</span>
            </h3>
            {{-- <div class="card-toolbar">
                <button data-bs-toggle="modal" data-bs-target="#createData" class="btn btn-primary align-self-center" data-kt-menu-placement="bottom-end">Add Education</button>
            </div> --}}
        </div>
        <div class="card-body pt-5">
            <div class="timeline-label">
                <div class="timeline-item">
                    @php
                        $education = $user->education()->first();
                    @endphp
                    @if (isset($education))
                    <div class="timeline-label fw-bolder text-gray-800 fs-6"></div>
                    <div class="timeline-badge">
                        <i class="fa fa-genderless text-warning fs-1"></i>
                    </div>
                    <div class="timeline-content d-flex">
                        <span class="fw-bolder text-gray-800 ps-3">{{$education->fakultas->name . ' - ' . $education->jurusan->name . ' - ' . $education->program_studi->name . ' (' . $education->tingkat_pendidikan->name .')'}}</span>
                        <span class="fw-bolder text-gray-800 ps-3">{{$education->nim}}</span>
                        <span class="fw-bolder text-gray-800 ps-3">{{$education->year_start . ' - ' . $education->year_end}}</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
