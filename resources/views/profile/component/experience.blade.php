<div class="col-xl-6">
    <div class="card card-xl-stretch mb-5 mb-xl-10">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1">Experiences</span>
                <span class="text-muted mt-1 fw-bold fs-7">My Experiences</span>
            </h3>
            {{-- <div class="card-toolbar">
                <button data-bs-toggle="modal" data-bs-target="#createData" class="btn btn-primary align-self-center" data-kt-menu-placement="bottom-end">Add Experience</button>
            </div> --}}
        </div>
        <div class="card-body py-3">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="kt_table_widget_5_tab_1">
                    <div class="table-responsive">
                        <table class="table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4">
                            <thead>
                                <tr class="border-0">
                                    <th class="p-0 w-50px"></th>
                                    <th class="p-0 min-w-150px"></th>
                                    <th class="p-0 min-w-140px"></th>
                                    <th class="p-0 min-w-110px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user->experience()->get() as $experience)
                                <tr>
                                    <td>
                                        <div class="symbol symbol-45px me-2">
                                            <span class="symbol-label">
                                                <img src="{{URL('assets/dist/assets/media/icons/duotune/abstract/abs014.svg')}}"/>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">{{$experience->nama_perusahaan}}</a>
                                        <span class="text-muted fw-bold d-block">{{$experience->tanggal_masuk . ' ' . $experience->tanggal_keluar}}</span>
                                    </td>
                                    <td class="text-end text-muted fw-bold">>{{$experience->jabatan->name}}</td>
                                    <td class="text-end">
                                        <span class="badge badge-light-success">{{$experience->description}}</span>
                                    </td>
                                </tr>
                                @endforeach


                                <tr>
                                    <td>
                                        <div class="symbol symbol-45px me-2">
                                            <span class="symbol-label">
                                                <img src="{{URL('assets/dist/assets/media/icons/duotune/abstract/abs014.svg')}}"/>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">PT. ABCDE</a>
                                        <span class="text-muted fw-bold d-block">2012 - 2020</span>
                                    </td>
                                    <td class="text-end text-muted fw-bold">Web Dev</td>
                                    <td class="text-end">
                                        <span class="badge badge-light-success">Success</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
