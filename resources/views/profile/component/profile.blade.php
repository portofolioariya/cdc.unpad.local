<div class="profiles">
    <div class="card-header cursor-pointer">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Profile Details</h3>
        </div>
        {{-- <a href="../../demo2/dist/account/settings.html" class="btn btn-primary align-self-center">Edit Profile</a> --}}
    </div>
    <div class="card-body p-9">
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Full Name</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">Ikhwanu Arriyadh Muhammad</span>
            </div>
        </div>
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Username</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">olaolaol118</span>
            </div>
        </div>
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Email</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">olaolaol118</span>
            </div>
        </div>
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Place, Date of birth</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">Kuningan, 19 Mai 1998</span>
            </div>
        </div>
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Gender</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">Male</span>
            </div>
        </div>
        <div class="row mb-7">
            <label class="col-lg-4 fw-bold text-muted">Job Status</label>
            <div class="col-lg-8">
                <span class="fw-bolder fs-6 text-gray-800">Active</span>
            </div>
        </div>
        <div class="notice d-flex bg-light-success rounded border-success border border-dashed p-6">
            <div class="d-flex flex-stack flex-grow-1">
                <div class="fw-bold">
                    <h4 class="text-gray-900 fw-bolder">About Me</h4>
                    <div class="fs-6 text-gray-700">
                        Madison Blackstone is a director of brand marketing, with experience managing global teams and multi-million-dollar campaigns. Her background in brand strategy, visual design, and account management inform her mindful but competitive approach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
