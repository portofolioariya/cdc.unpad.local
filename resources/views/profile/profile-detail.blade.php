@extends('layouts.base-layout.content.base-layout')
@push('title', 'Test')
@section('content')
<div class="">
    <div class="card mb-5 mb-xl-10">
        <div class="card-body pt-9 pb-0">
            @include('profile.component.overview')
            <div class="d-flex overflow-auto h-55px">
                <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 active" href="../../demo2/dist/account/overview.html">Profile</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
        @include('profile.component.profile')
    </div>
    <div class="row gy-5 g-xl-10">
        @include('profile.component.skill')
        @include('profile.component.experience')
    </div>
    <div class="row gy-5 gx-xl-10">
        @include('profile.component.education')
        @include('profile.component.kontak')
    </div>
    {{-- @include('profile.modals.example-modal') --}}
</div>
@endsection
