@extends('layouts.auth-layout.content.auth-layout')

@section('content')
<div class="w-lg-600px p-10 p-lg-15 mx-auto">
    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{URL('register')}}" method="POST">
        @csrf
        @method('POST')
        <div class="text-center mb-10">
            <h1 class="text-dark mb-3">Create an Account</h1>
            <div class="text-gray-400 fw-bold fs-4">Already have an account?
            <a href="{{URL('login')}}" class="link-primary fw-bolder">Sign in here</a></div>
        </div>

        @if (Session::has('status'))
        <div class="row fv-row mb-7 badge badge-light-{{Session::get('status')}}">
            <div class="col-xl-12">
                <span class="form-label fw-bolder text-{{Session::get('status')}} fs-6"> {{Session::get('messages')}} </span>
            </div>
        </div>
        @endif

        <div class="row fv-row mb-7">
            <div class="col-xl-6">
                <label class="form-label fw-bolder text-dark fs-6">First Name  @error('first_name') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="first_name" value="{{old('first_name')}}" autocomplete="off" />
                @error('first_name')
                    <div class="fv-plugins-message-container invalid-feedback">
                        <div data-field="first_name" data-validator="first_name">
                            {{ $message }}
                        </div>
                    </div>
                @enderror
            </div>
            <div class="col-xl-6">
                <label class="form-label fw-bolder text-dark fs-6">Last Name  @error('last_name') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="last_name" value="{{old('last_name')}}" autocomplete="off" />
                @error('last_name')
                    <div class="fv-plugins-message-container invalid-feedback">
                        <div data-field="last_name" data-validator="last_name">
                            {{ $message }}
                        </div>
                    </div>
                @enderror
            </div>
        </div>
        <div class="fv-row mb-7">
            <label class="form-label fw-bolder text-dark fs-6">Username @error('username') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
            <input class="form-control form-control-lg form-control-solid" type="username" placeholder="" name="username" value="{{old('username')}}" autocomplete="off" />
            @error('username')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="username" data-validator="username">
                        {{ $message }}
                    </div>
                </div>
            @enderror
        </div>
        <div class="fv-row mb-7">
            <label class="form-label fw-bolder text-dark fs-6">Email @error('email') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="" name="email" value="{{old('email')}}" autocomplete="off" />
            @error('email')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="email" data-validator="email">
                        {{ $message }}
                    </div>
                </div>
            @enderror
        </div>
        <div class="mb-10 fv-row" data-kt-password-meter="true">
            <div class="mb-1">
                <label class="form-label fw-bolder text-dark fs-6">Password @error('password') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
                <div class="position-relative mb-3">
                    <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="password" value="{{old('password')}}" autocomplete="off" />
                    <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                        <i class="bi bi-eye-slash fs-2"></i>
                        <i class="bi bi-eye fs-2 d-none"></i>
                    </span>
                </div>
                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                </div>
            </div>
            <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
            @error('password')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="password" data-validator="password">
                        {{ $message }}
                    </div>
                </div>
            @enderror

        </div>
        <div class="fv-row mb-5">
            <label class="form-label fw-bolder text-dark fs-6">Confirm Password @error('password_confirmation') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
            <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="password_confirmation" autocomplete="off" />
            @error('password_confirmation')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="password_confirmation" data-validator="password_confirmation">
                        {{ $message }}
                    </div>
                </div>
            @enderror
        </div>
        <div class="text-center">
            <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                <span class="indicator-label">Sign Up</span>
            </button>
        </div>
    </form>
</div>
@endsection


{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
