@extends('layouts.auth-layout.content.auth-layout')

@section('content')
<div class="w-lg-500px p-10 p-lg-15 mx-auto">

    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{{URL('login')}}" method="POST">
        @csrf
        @method('POST')
        <div class="text-center mb-10">
            <h1 class="text-dark mb-3">Sign In to Metronic</h1>
            <div class="text-gray-400 fw-bold fs-4">New Here?
            <a href="{{URL('register')}}" class="link-primary fw-bolder">Create an Account</a></div>
        </div>
        @if (Session::has('status'))
        <div class="row fv-row mb-7">
            <div class="col-xl-6 badge badge-light-{{Session::get('status')}}">
                <span class="form-label fw-bolder text-{{Session::get('status')}} fs-6"> {{Session::get('messages')}} </span>
            </div>
        </div>
        @endif

        <div class="fv-row mb-10">
            <label class="form-label fs-6 fw-bolder text-dark">E-Mail atau Username @error('login') <i class="text-danger fas fa-exclamation-circle"></i> @enderror </label>
            <input class="form-control form-control-lg form-control-solid" type="text" name="login" value="{{old('login')}}" autocomplete="off" />
            @error('login')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="login" data-validator="login">
                        {{ $message }}
                    </div>
                </div>
            @enderror
        </div>
        <div class="fv-row mb-10">
            <div class="d-flex flex-stack mb-2">
                <label class="form-label fw-bolder text-dark fs-6 mb-0">Password @error('password') <i class="text-danger fas fa-exclamation-circle"></i> @enderror</label>
                {{-- <a href="../../demo2/dist/authentication/flows/aside/password-reset.html" class="link-primary fs-6 fw-bolder">Forgot Password ?</a> --}}
            </div>
            <input class="form-control form-control-lg form-control-solid" type="password" name="password" value="{{old('password')}}" autocomplete="off" />
            @error('password')
                <div class="fv-plugins-message-container invalid-feedback">
                    <div data-field="password" data-validator="password">
                        {{ $message }}
                    </div>
                </div>
            @enderror
        </div>
        <div class="fv-row mb-10">
            <input id="remember-me" type="checkbox" name="remember" class="form-check-input border mr-2">
            <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
        </div>
        <div class="text-center">
            <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                <span class="indicator-label">Sign In</span>
            </button>
        </div>
    </form>
</div>
@endsection

{{-- 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
