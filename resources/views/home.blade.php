@extends('layouts.base-layout.content.base-layout')
@push('title', 'Test')
@section('content')
<div class="">
    <div class="card mb-5 mb-xl-10">
        <div class="card-body pt-9 pb-0">
            <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                <div class="me-7 mb-4">
                    <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                        <img src="" alt="image" />
                        {{-- <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div> --}}
                    </div>
                </div>
                <div class="flex-grow-1">
                    <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                        <div class="d-flex flex-column">
                            <div class="d-flex align-items-center mb-2">
                                <a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bolder me-1">Nama Lengkap</a>
                            </div>
                            <div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                                <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="black" />
                                        <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="black" />
                                    </svg>
                                </span>
                               status job </a>
                                <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="black" />
                                        <path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="black" />
                                    </svg>
                                </span>
                               adress</a>
                                <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z" fill="black" />
                                        <path d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z" fill="black" />
                                    </svg>
                                </span>
                                Email</a>
                            </div>
                        </div>
                        <div class="d-flex my-4">
                            <a href="#" class="btn btn-sm btn-primary me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_offer_a_deal">Send me a message</a>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap flex-stack">
                        <div class="d-flex flex-column flex-grow-1 pe-8">
                            <div class="d-flex flex-wrap">
                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                    <div class="d-flex align-items-center">
                                        <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="">34</div>
                                    </div>
                                    <div class="fw-bold fs-6 text-gray-400">Experience</div>
                                </div>
                                <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                    <div class="d-flex align-items-center">
                                        <div class="fs-2 fw-bolder" data-kt-countup="true" data-kt-countup-value="xx">09</div>
                                    </div>
                                    <div class="fw-bold fs-6 text-gray-400">Skills</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex overflow-auto h-55px">
                <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 active" href="../../demo2/dist/account/overview.html">Edit Profile</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>




    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Profile Details</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="row mb-6">
                        <label class="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
                        <div class="col-lg-8">
                            <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(assets/media/avatars/blank.png)">
                                <div class="image-input-wrapper w-125px h-125px" style="background-image: url(assets/media/avatars/150-26.jpg)"></div>
                                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                    <i class="bi bi-pencil-fill fs-7"></i>
                                    <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                                    <input type="hidden" name="avatar_remove" />
                                </label>
                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                    <i class="bi bi-x fs-2"></i>
                                </span>
                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                    <i class="bi bi-x fs-2"></i>
                                </span>
                            </div>
                            <div class="form-text">Allowed file types: png, jpg, jpeg.</div>
                        </div>
                    </div>
                    <div class="row mb-6">
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">First Name</label>
                        <div class="col-lg-8 fv-row">
                            <input type="text" name="company" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="Keenthemes" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Last Name</label>
                        <div class="col-lg-8 fv-row">
                            <input type="text" name="company" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="Keenthemes" />
                        </div>
                    </div>
                    <div class="row mb-6">
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                        <div class="col-lg-8 fv-row">
                            <input type="text" name="company" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="Keenthemes" />
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>





    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Skills</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="row skillGroup">
                        <div class="col-xl-4">
                                <div class="fv-row mb-10">
                                    <label class="required fw-bold fs-6 mb-2">Nama Skill</label>
                                    <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Jenis Skill</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-3 addDeleteSkill">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Level</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 deleteSkill" style="display: none">
                        <div class="fv-row mb-10">
                            <a id="removeSkill" type="reset" class="btn btn-light-danger me-2" style="margin-top: 28px">Delete</a>
                        </div>
                    </div>
                    <div>
                        <a class="btn btn-light me-2 addRowSkill">Add Row</a>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>





    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Experience</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="expGroup">
                        <div class="row">
                            <div class="col-xl-5">
                                    <div class="fv-row mb-10">
                                        <label class="required fw-bold fs-6 mb-2">Nama Perusahaan</label>
                                        <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                    </div>
                            </div>
                            <div class="col-xl-5">
                                <div class="fv-row mb-10">
                                    <label class="required fw-bold fs-6 mb-2">Jabatan</label>
                                    <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                        <option></option>
                                        <option value="1">Option 1</option>
                                        <option value="2">Option 2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-5">
                                    <div class="fv-row mb-10">
                                        <label class="required fw-bold fs-6 mb-2">Tanggal Masuk</label>
                                        <input type="date" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                    </div>
                            </div>
                            <div class="col-xl-5 addDeleteExp">
                                <div class="fv-row mb-10">
                                    <label class="required fw-bold fs-6 mb-2">Tanggal Keluar</label>
                                    <input type="date" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                                </div>
                            </div>
                            <div class="col-xl-2 deleteExp" style="display: none">
                                <div class="fv-row mb-10">
                                    <a id="removeExp" type="reset" class="btn btn-light-danger me-2" style="margin-top: 28px">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a class="btn btn-light me-2 addRowExperience">Add Row</a>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>






    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Education</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Tingkat Pendidikan</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Fakultas</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Jurusan</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">IPK</label>
                                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Tahun Masuk</label>
                                <input type="number" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Tahun Keluar</label>
                                <input type="number" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>





    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Contact</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="row contactGroup">
                        <div class="col-xl-5">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Social Media</label>
                                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="col-xl-5 addDeleteContact">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Account Link</label>
                                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="col-xl-2 deleteContact" style="display: none">
                            <div class="fv-row mb-10">
                                <a id="removeContact" type="reset" class="btn btn-light-danger me-2" style="margin-top: 28px">Delete</a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a class="btn btn-light me-2 addRowContact">Add Row</a>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>





    <div class="card mb-5 mb-xl-10">
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Perusahaan</h3>
            </div>
        </div>
        <div id="kt_account_profile_details" class="collapse show">
            <form id="kt_account_profile_details_form" class="form">
                <div class="card-body border-top p-9">
                    <div class="row">
                        <div class="col-xl-5">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Nama Perusahaan</label>
                                <input type="number" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="col-xl-5">
                            <div class="fv-row mb-10">
                                <label class="required fw-bold fs-6 mb-2">Jabatan</label>
                                <select class="form-select" data-control="select2" data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
                </div>
            </form>
        </div>
    </div>



</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".addRowSkill").click(function () {
            var fieldHTML = '<div class="row skillGroup">' + $(".skillGroup").html();
            $('body').find('.skillGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteSkill">' + $(".deleteSkill").html() + '</div>';
            $('body').find('.addDeleteSkill:last').after(btnRemove);
        });
        $("body").on("click", "#removeSkill", function () {
            let currow = $(this).closest('.skillGroup');
            $(this).parents(".skillGroup").remove();
        });




        $(".addRowExperience").click(function () {
            var fieldHTML = '<div class="expGroup">' + $(".expGroup").html();
            $('body').find('.expGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteExp">' + $(".deleteExp").html() + '</div>';
            $('body').find('.addDeleteExp:last').after(btnRemove);
        });
        $("body").on("click", "#removeExp", function () {
            let currow = $(this).closest('.expGroup');
            $(this).parents(".expGroup").remove();
        });



        $(".addRowContact").click(function () {
            var fieldHTML = '<div class="row contactGroup">' + $(".contactGroup").html();
            $('body').find('.contactGroup:last').after(fieldHTML);
            var btnRemove = '<div class="col-xl-2 deleteContact">' + $(".deleteContact").html() + '</div>';
            $('body').find('.addDeleteContact:last').after(btnRemove);
        });
        $("body").on("click", "#removeContact", function () {
            let currow = $(this).closest('.contactGroup');
            $(this).parents(".contactGroup").remove();
        });
    });

</script>
@endsection
