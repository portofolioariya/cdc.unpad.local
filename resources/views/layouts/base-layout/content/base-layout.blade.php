<!DOCTYPE html>
<html lang="en">
    <head><base href="">
        <title>@stack('title')</title>
        @include('layouts.base-layout.component.head')
        @stack('head')
    </head>

	<body id="kt_body" style="background-color: #F2F5F8" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
		<div class="d-flex flex-column flex-root">
			<div class="page d-flex flex-row flex-column-fluid">
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					@include('layouts.base-layout.component.header')
                    <div class="">
                        <div class="card" style="background-color: #fff" >
                            <div class="card-body">
                                <div class="toolbar" id="kt_toolbar">
                                    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
                                        <div class="page-title d-flex flex-column me-3">
                                            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">@stack('title')</h1>
                                            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                                @include('layouts.base-layout.component.breadcumb')
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5">
                        <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
                            <div class="content flex-row-fluid" id="kt_content">
                                @include('layouts.base-layout.component.alert')
                                @yield('content')
                            </div>
                        </div>
                    </div>
					@include('layouts.base-layout.component.footer')
				</div>
			</div>
		</div>
		@include('layouts.base-layout.component.scroll-top')
		@include('layouts.base-layout.component.js')
	</body>
</html>
