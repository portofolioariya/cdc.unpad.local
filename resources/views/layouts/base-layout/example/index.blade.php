@extends('layouts.base-layout.content.base-layout')
@push('title', 'Test')
@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <div class="row gx-5 gx-xl-8 mb-5 mb-xl-8">
            <div class="col-xxl-4">
                <div class="card card-xxl-stretch bg-primary">
                    <div class="card-body">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com013.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black"/>
                            <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black"/>
                            </svg>
                        </span>
                            <!--end::Svg Icon-->
                        <div class="d-flex flex-column">
                            <div class="text-white fw-bolder fs-1 mb-0 mt-5">790</div>
                            <div class="text-white fw-bold fs-6">Jumlah Mahasiswa</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4">
                <div class="card card-xxl-stretch bg-warning">
                    <div class="card-body">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com013.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black"/>
                            <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black"/>
                            </svg>
                        </span>
                            <!--end::Svg Icon-->
                        <div class="d-flex flex-column">
                            <div class="text-white fw-bolder fs-1 mb-0 mt-5">790</div>
                            <div class="text-white fw-bold fs-6">Alumni Open to Work</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4">
                <div class="card card-xxl-stretch bg-success">
                    <div class="card-body">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com013.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black"/>
                            <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black"/>
                            </svg>
                        </span>
                            <!--end::Svg Icon-->
                        <div class="d-flex flex-column">
                            <div class="text-white fw-bolder fs-1 mb-0 mt-5">790</div>
                            <div class="text-white fw-bold fs-6">Work</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row gy-5 g-xl-8">
            <div class="col-xxl-6">
                <div class="card card-xxl-stretch">
                    <div class="card-header align-items-center border-0 mt-4">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-2 text-dark">IPK Tertinggi</span>
                            <span class="text-muted fw-bold fs-7">Alumni dengan IPK tertinggi</span>
                        </h3>
                    </div>
                    <div class="card-body pt-5">
                        <div class="timeline-label">
                            <div class="timeline-item">
                                <div class="timeline-label fw-bolder text-gray-800 fs-6">3.09</div>
                                <div class="timeline-badge">
                                    <i class="fa fa-genderless text-primary fs-1"></i>
                                </div>
                                <div class="fw-mormal timeline-content text-muted ps-3">Muhammad Kiki</div>
                            </div>
                            <div class="timeline-item">
                                <div class="timeline-label fw-bolder text-gray-800 fs-6">3.09</div>
                                <div class="timeline-badge">
                                    <i class="fa fa-genderless text-warning fs-1"></i>
                                </div>
                                <div class="fw-mormal timeline-content text-muted ps-3">Muhammad Rizki</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-6">
                <div class="card card-xxl-stretch mb-xl-3">
                    <div class="card-header border-0">
                        <h3 class="card-title fw-bolder text-dark">Data Terbaru</h3>
                    </div>
                    <div class="card-body pt-2">
                        <div class="d-flex align-items-center mb-8">
                            <span class="bullet bullet-vertical h-40px bg-success"></span>
                            <div class="form-check form-check-custom form-check-solid mx-5">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                            <div class="flex-grow-1">
                                <a href="#" class="text-gray-800 text-hover-primary fw-bolder fs-6">Farhan Rizki R</a>
                                <span class="text-muted fw-bold d-block">email@gmail.com</span>
                            </div>
                            <span class="badge badge-light-success fs-8 fw-bolder">New</span>
                        </div>
                        <div class="d-flex align-items-center mb-8">
                            <span class="bullet bullet-vertical h-40px bg-primary"></span>
                            <div class="form-check form-check-custom form-check-solid mx-5">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                            <div class="flex-grow-1">
                                <a href="#" class="text-gray-800 text-hover-primary fw-bolder fs-6">Farhan Rizki R</a>
                                <span class="text-muted fw-bold d-block">email@gmail.com</span>
                            </div>
                            <span class="badge badge-light-primary fs-8 fw-bolder">New</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row gy-5 g-xl-8">
            <div class="col-xl-12">
                <div class="card card-xxl-stretch mb-5 mb-xl-8">
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">Alumni Terpopuler</span>
                            <span class="text-muted mt-1 fw-bold fs-7">Dari 500 alumni</span>
                        </h3>
                    </div>
                    <div class="card-body py-3">
                        <div class="table-responsive">
                            <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                                <thead>
                                    <tr class="fw-bolder text-muted">
                                        <th class="min-w-150px">Nama Lengkap</th>
                                        <th class="min-w-140px">Prodi</th>
                                        <th class="min-w-120px">Jusrusan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex justify-content-start flex-column">
                                                    <a href="#" class="text-dark fw-bolder text-hover-primary fs-6">Farhan Rizki R</a>
                                                    <span class="text-muted fw-bold text-muted d-block fs-7">email@gmail.com</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">Teknik dan Ilmu Komputer</a>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">Teknik Informatika</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
