<div class="d-flex align-items-stretch" id="kt_header_nav">
    <!--begin::Menu wrapper-->
    <div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
        <!--begin::Menu-->
        <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true">
            <div  class="menu-item menu-lg-down-accordion me-lg-1">
                <a class="menu-link
                {{\App\Services\Menu\ActiveMenu::is_active('dashboard')}}
                py-3" href="{{URL('dashboard')}}">
                    {{-- <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span> --}}
                    <span class="menu-title">Dashboard</span>
                </a>
            </div>









            <div  class="menu-item menu-lg-down-accordion me-lg-1">
                <a class="menu-link
                {{\App\Services\Menu\ActiveMenu::is_active('list-alumni')}}
                py-3" href="{{URL('list-alumni')}}">
                    {{-- <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span> --}}
                    <span class="menu-title">List Alumni</span>
                </a>
            </div>




            <div  class="menu-item menu-lg-down-accordion me-lg-1">
                <a class="menu-link
                {{\App\Services\Menu\ActiveMenu::is_active('list-perusahaan')}}
                py-3" href="{{URL('list-perusahaan')}}">
                    {{-- <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span> --}}
                    <span class="menu-title">List Perusahaan</span>
                </a>
            </div>


            








            <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                <a class="menu-link py-3" href="#">
                    <span class="menu-title">Ticketing</span>
                    <span class="menu-arrow d-lg-none"></span>
                </a>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                    <div class="menu-item">
                        <a class="menu-link py-3" href="{{ URL('ticketing') }}">
                            <span class="menu-icon">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen009.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M21 22H14C13.4 22 13 21.6 13 21V3C13 2.4 13.4 2 14 2H21C21.6 2 22 2.4 22 3V21C22 21.6 21.6 22 21 22Z" fill="black" />
                                        <path d="M10 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H10C10.6 2 11 2.4 11 3V21C11 21.6 10.6 22 10 22Z" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="menu-title">Ticketing</span>
                        </a>
                    </div>
                </div>
            </div>

            @can('rbac')
            <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                <span class="menu-link
                @php
                    $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%rbac%')->get();
                @endphp
                @foreach ($permissions as $permission)
                    {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                @endforeach
                 py-3">
                    <span class="menu-title">RBAC</span>
                    <span class="menu-arrow d-lg-none"></span>
                </span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                    @can('rbac-permission')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <div class="menu-item">
                            <a class="menu-link
                            {{\App\Services\Menu\ActiveMenu::is_active('rbac-permission')}}
                            py-3" href="{{URL('rbac-permission')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Permission</span>
                            </a>
                        </div>
                    </div>
                    @endcan

                    @can('rbac-role')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <div class="menu-item">
                            <a class="menu-link
                            {{\App\Services\Menu\ActiveMenu::is_active('rbac-role')}}
                            py-3" href="{{URL('rbac-role')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Role</span>
                            </a>
                        </div>
                    </div>
                    @endcan

                    @can('rbac-user')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <div class="menu-item">
                            <a class="menu-link
                            {{\App\Services\Menu\ActiveMenu::is_active('rbac-user')}}
                            py-3" href="{{URL('rbac-user')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">User</span>
                            </a>
                        </div>
                    </div>
                    @endcan

                </div>
            </div>
            @endcan

            @can('master')
            <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                <span class="menu-link
                @php
                    $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master%')->get();
                @endphp
                @foreach ($permissions as $permission)
                    {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                @endforeach
                 py-3">
                    <span class="menu-title">Master</span>
                    <span class="menu-arrow d-lg-none"></span>
                </span>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                    @can('master-user')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <span class="menu-link
                        @php
                            $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-user%')->get();
                        @endphp
                        @foreach ($permissions as $permission)
                            {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                        @endforeach
                        py-3">
                            <span class="menu-icon">
                                <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm007.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
                                        <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="menu-title">User</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown  py-lg-4 w-lg-225px">
                            @can('master-user-gender')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <div class="menu-item">
                                    <a class="menu-link
                                    @php
                                        $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-user-gender%')->get();
                                    @endphp
                                    @foreach ($permissions as $permission)
                                        {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                                    @endforeach
                                    py-3" href="{{URL('master-user-gender')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Jenis Kelamin</span>
                                    </a>
                                </div>
                            </div>
                            @endcan

                            @can('master-user-user type')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <div class="menu-item">
                                    <a class="menu-link
                                    {{\App\Services\Menu\ActiveMenu::is_active('master-user-user-type')}}
                                    py-3" href="{{URL('master-user-user-type')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">User Type</span>
                                    </a>
                                </div>
                            </div>
                            @endcan

                            @can('master-user-mahasiswa')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <span class="menu-link
                                @php
                                    $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-user-mahasiswa%')->get();
                                @endphp
                                @foreach ($permissions as $permission)
                                    {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                                @endforeach
                                py-3">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Mahasiswa</span>
                                    <span class="menu-arrow"></span>
                                </span>

                                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown py-lg-4 w-lg-225px">
                                    @can('master-user-mahasiswa-fakultas')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mahasiswa-fakultas')}}
                                        py-3" href="{{URL('master-user-mahasiswa-fakultas')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Fakultas</span>
                                        </a>
                                    </div>
                                    @endcan
                                    @can('master-user-mahasiswa-jurusan')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mahasiswa-jurusan')}}
                                        py-3" href="{{URL('master-user-mahasiswa-jurusan')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Jurusan</span>
                                        </a>
                                    </div>
                                    @endcan
                                    @can('master-user-mahasiswa-program studi')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mahasiswa-program-studi')}}
                                        py-3" href="{{URL('master-user-mahasiswa-program-studi')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Program Studi</span>
                                        </a>
                                    </div>
                                    @endcan
                                    @can('master-user-mahasiswa-tingkat pendidikan')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mahasiswa-tingkat-pendidikan')}}
                                        py-3"  href="{{URL('master-user-mahasiswa-tingkat-pendidikan')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Tingkat Pendidikan</span>
                                        </a>
                                    </div>
                                    @endcan
                                </div>
                            </div>
                            @endcan
                            @can('master-user-mitra unpad')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <span class="menu-link
                                @php
                                    $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-user-mitra unpad%')->get();
                                @endphp
                                @foreach ($permissions as $permission)
                                    {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                                @endforeach
                                py-3">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Mitra Unpad</span>
                                    <span class="menu-arrow"></span>
                                </span>
                                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown  py-lg-4 w-lg-225px">
                                    @can('master-user-mitra unpad-jabatan')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mitra-unpad-jabatan')}}
                                        py-3" href="{{URL('master-user-mitra-unpad-jabatan')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Jabatan</span>
                                        </a>
                                    </div>
                                    @endcan
                                    @can('master-user-mitra unpad-jenis perusahaan')
                                    <div class="menu-item">
                                        <a class="menu-link
                                        {{\App\Services\Menu\ActiveMenu::is_active('master-user-mitra-unpad-jenis-perusahaan')}}
                                        py-3" href="{{URL('master-user-mitra-unpad-jenis-perusahaan')}}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">Jenis Perusahaan</span>
                                        </a>
                                    </div>
                                    @endcan
                                    @can('master-user-mitra unpad-perusahaan')
                                        <div class="menu-item">
                                            <a class="menu-link
                                    {{\App\Services\Menu\ActiveMenu::is_active('master-user-mitra-unpad-perusahaan')}}
                                                py-3" href="{{URL('master-user-mitra-unpad-perusahaan')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                                <span class="menu-title">Perusahaan</span>
                                            </a>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            @endcan
                        </div>
                    </div>
                    @endcan
                    @can('master-address')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <span class="menu-link
                        @php
                            $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-address%')->get();
                        @endphp
                        @foreach ($permissions as $permission)
                            {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                        @endforeach
                        py-3">
                            <span class="menu-icon">
                                <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm007.svg-->
                                <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M18.0624 15.3454L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3454C4.56242 13.6454 3.76242 11.4452 4.06242 8.94525C4.56242 5.34525 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24525 19.9624 9.94525C20.0624 12.0452 19.2624 13.9454 18.0624 15.3454ZM13.0624 10.0453C13.0624 9.44534 12.6624 9.04534 12.0624 9.04534C11.4624 9.04534 11.0624 9.44534 11.0624 10.0453V13.0453H13.0624V10.0453Z" fill="black"/>
                                    <path d="M12.6624 5.54531C12.2624 5.24531 11.7624 5.24531 11.4624 5.54531L8.06241 8.04531V12.0453C8.06241 12.6453 8.46241 13.0453 9.06241 13.0453H11.0624V10.0453C11.0624 9.44531 11.4624 9.04531 12.0624 9.04531C12.6624 9.04531 13.0624 9.44531 13.0624 10.0453V13.0453H15.0624C15.6624 13.0453 16.0624 12.6453 16.0624 12.0453V8.04531L12.6624 5.54531Z" fill="black"/>
                                    </svg></span>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="menu-title">Address</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown  py-lg-4 w-lg-225px">
                            @can('master-address-province')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <div class="menu-item">
                                    <a class="menu-link
                                    {{\App\Services\Menu\ActiveMenu::is_active('master-address-province')}}
                                        py-3" href="{{URL('master-address-province')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Province</span>
                                    </a>
                                </div>
                            </div>
                            @endcan
                            @can('master-address-city')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <div class="menu-item">
                                    <a class="menu-link
                                    {{\App\Services\Menu\ActiveMenu::is_active('master-address-city')}}
                                        py-3" href="{{URL('master-address-city')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">City</span>
                                    </a>
                                </div>
                            </div>
                            @endcan
                        </div>
                    </div>
                    @endcan
                    @can('master-ticketing')
                    <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                        <span class="menu-link
                        @php
                            $permissions = \Spatie\Permission\Models\Permission::where('name', 'like', '%master-ticketing%')->get();
                        @endphp
                        @foreach ($permissions as $permission)
                            {{\App\Services\Menu\ActiveMenu::is_active($permission->name)}}
                        @endforeach
                        py-3">
                            <span class="menu-icon">
                                <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm007.svg-->
                                <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z" fill="black"/>
                                    <path d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z" fill="black"/>
                                    </svg></span>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="menu-title">Ticketing</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown  py-lg-4 w-lg-225px">
                            @can('master-ticketing-kategori')
                            <div data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-placement="right-start" class="menu-item menu-lg-down-accordion">
                                <div class="menu-item">
                                    <a class="menu-link
                                    {{\App\Services\Menu\ActiveMenu::is_active('master-ticketing-kategori')}}
                                        py-3" href="{{URL('master-ticketing-kategori')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Kategori</span>
                                    </a>
                                </div>
                            </div>
                            @endcan
                        </div>
                    </div>
                    @endcan
                </div>
            </div>
            @endcan
        </div>
    </div>
</div>
