<div class="d-flex align-items-center me-n3 ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
    <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
        <img class="h-30px w-30px rounded" src="{{URL(isset(Auth::user()->profile()->images) ?Auth::user()->profile()->images :'assets/dist/assets/media/avatars/blank.png')}}" alt="" />
    </div>
    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
        <div class="menu-item px-3">
            <div class="menu-content d-flex align-items-center px-3">
                <div class="symbol symbol-50px me-5">
                    <img alt="Logo" src="{{URL(isset(Auth::user()->profile()->images) ?Auth::user()->profile()->images :'assets/dist/assets/media/avatars/blank.png')}}" />
                </div>
                <div class="d-flex flex-column">
                    <div class="fw-bolder d-flex align-items-center fs-5">{{Auth::user()->first_name . ' ' . Auth::user()->last_name }}
                    <span class="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">{{Auth::user()->getRoleNames()[0] }}</span>
                    </div>
                    <a href="#" class="fw-bold text-muted text-hover-primary fs-7">{{Auth::user()->email }}</a>
                </div>
            </div>
        </div>
        <div class="separator my-2"></div>
        <div class="menu-item px-5">
            <a href="{{URL('/profile/'.\Illuminate\Support\Facades\Crypt::encrypt(Auth::user()->id))}}" class="menu-link px-5">My Profile</a>
        </div>

        <div class="menu-item px-5 my-1">
            <a href="{{URL('/account-setting/'.\Illuminate\Support\Facades\Crypt::encrypt(Auth::user()->id))}}" class="menu-link px-5">Account Settings</a>
        </div>
        <div class="menu-item px-5">
            <a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="menu-link px-5">Sign Out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
        <div class="separator my-2"></div>
        {{-- @include('layouts.base-layout.component.dark-mode') --}}
    </div>
</div>
