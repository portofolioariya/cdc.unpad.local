
<meta name="description" content="CDC UNPAD" />
<meta name="keywords" content="CDC UNPAD" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta charset="utf-8" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="CDC UNPAD" />
<meta property="og:url" content="CDC UNPAD" />
<meta property="og:site_name" content="CDC UNPAD" />
<link rel="shortcut icon" href="{{URL('assets/dist/assets/media/unpad.png')}}" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link href="{{URL('assets/dist/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL('assets/dist/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL('assets/dist/assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL('assets/dist/assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css"> --}}
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css ">
