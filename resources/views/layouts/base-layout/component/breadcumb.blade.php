@php
$breadcrumbs = \App\Services\Menu\BreadCumb::breadcrumbMenu();
$previous_breadcrumb = null;
@endphp
@foreach ( $breadcrumbs as $breadcrumb)
@if ($breadcrumb['status'] == true)
    @if ($breadcrumb == end($breadcrumbs))
    <li class="breadcrumb-item text-dark opacity-75">
        <a href="{{URL($breadcrumb['url'])}}" class="breadcrumb--active">{{$breadcrumb['name']}}</a> 
    </li>
    {{-- <i data-feather="chevron-right" class="breadcrumb__icon"></i> --}}
    @else
    <li class="breadcrumb-item text-dark opacity-75">
        <a href="{{URL($breadcrumb['url'])}}" class="text-dark opacity-75">{{$breadcrumb['name']}}</a> 
    </li>
    <li class="breadcrumb-item">
        <span class="bullet bg-dark opacity-75 w-5px h-2px"></span>
    </li>
    @endif
    @php
        $previous_breadcrumb = $breadcrumb;
    @endphp
@else
    @if ($breadcrumb == end($breadcrumbs))
    <li class="breadcrumb-item text-dark opacity-75">
        <a href="{{URL($breadcrumb['url'])}}" class="breadcrumb--active">{{$previous_breadcrumb['name']}}</a> 
    </li>
    {{-- <i data-feather="chevron-right" class="breadcrumb__icon"></i> --}}
    @endif
@endif
@endforeach 

{{-- <li class="breadcrumb-item text-dark opacity-75">
    <a href="#" class="text-dark text-hover-primary">Home</a>
</li>
<li class="breadcrumb-item">
    <span class="bullet bg-dark opacity-75 w-5px h-2px"></span>
</li>
<li class="breadcrumb-item text-dark opacity-75">Dashboard</li> --}}