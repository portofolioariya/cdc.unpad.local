<div class="d-flex flex-column flex-lg-row-auto w-xl-600px positon-xl-relative" style="background-color: #F2C98A">
    <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-600px scroll-y">
        <div class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
            <a href="../../demo2/dist/index.html" class="py-9 mb-5">
                <img alt="Logo" src="{{URL('assets/dist/assets/media/unpad.png')}}" class="h-150px" />
            </a>
            <h1 class="fw-bolder fs-2qx pb-5 pb-md-10" style="color: #986923;">Welcome to CDC Unpad</h1>
            <p class="fw-bold fs-2" style="color: #986923;">Discover Amazing CDC Unpad
            <br />with great build tools</p>
        </div>
        <div class="d-flex flex-row-auto bgi-no-repeat bgi-position-x-center bgi-size-contain bgi-position-y-bottom min-h-100px min-h-lg-250px" style="background-image: url({{URL('assets/dist/assets/media/illustrations/sigma-1/13.png')}}"></div>
    </div>
</div>
