<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@stack('title')</title>
        @include('layouts.auth-layout.component.head')
        @stack('head')
    </head>
	
	<body id="kt_body" class="bg-body">
		<div class="d-flex flex-column flex-root">
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				@include('layouts.auth-layout.content.welcome-side')
				<div class="d-flex flex-column flex-lg-row-fluid py-10">
					<div class="d-flex flex-center flex-column flex-column-fluid">
                        @yield('content')
					</div>
					@include('layouts.auth-layout.component.footer')
				</div>
			</div>
		</div>
		@include('layouts.auth-layout.component.js')
        @stack('script')
	</body>
</html>
