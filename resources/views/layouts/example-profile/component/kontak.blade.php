<div class="col-xl-6">
    <div class="card card-xl-stretch mb-5 mb-xl-10">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1">Contact</span>
                <span class="text-muted mt-1 fw-bold fs-7">You can call me in</span>
            </h3>
            <div class="card-toolbar">
                <button data-bs-toggle="modal" data-bs-target="#createData" class="btn btn-primary align-self-center" data-kt-menu-placement="bottom-end">Add Contact</button>
            </div>
        </div>
        <div class="card-body py-3">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="kt_table_widget_5_tab_1">
                    <div class="table-responsive">
                        <table class="table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4">
                            <thead>
                                <tr class="border-0">
                                    <th class="p-0 w-50px"></th>
                                    <th class="p-0 min-w-150px"></th>
                                    <th class="p-0 min-w-140px"></th>
                                    <th class="p-0 min-w-110px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="symbol symbol-45px me-2">
                                            <span class="symbol-label">
                                                <img src="assets/media/svg/brand-logos/plurk.svg" class="h-50 align-self-center" alt="" />
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">Instagram</a>
                                        <span class="text-muted fw-bold d-block">Movie Creator</span>
                                    </td>
                                    <td class="text-end text-muted fw-bold">Link</td>
                                    <td class="text-end">
                                        <span class="badge badge-light-success">Active</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
