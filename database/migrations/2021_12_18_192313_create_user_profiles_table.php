<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_province_id')->unsigned()->nullable();
            $table->foreign('m_province_id')->references('id')->on('m_provinces')->onDelete('cascade');
            $table->bigInteger('m_city_id')->unsigned()->nullable();
            $table->foreign('m_city_id')->references('id')->on('m_cities')->onDelete('cascade');
            $table->bigInteger('m_gender_id')->unsigned()->nullable();
            $table->foreign('m_gender_id')->references('id')->on('m_genders')->onDelete('cascade');
            $table->string('images')->nullable();
            $table->string('no_telp')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->longText('address')->nullable();
            $table->enum('status_pekerjaan',['bekerja', 'belum', 'pernah'])->nullable();
            $table->enum('status_pernikahan',['menikah', 'belum', 'pernah'])->nullable();
            $table->longText('about_me')->nullable();
            $table->enum('approved_status',['confirmed', 'unconfirmed', 'pending'])->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
