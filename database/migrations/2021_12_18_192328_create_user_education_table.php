<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_education', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_tingkat_pendidikan_id')->unsigned()->nullable();
            $table->foreign('m_tingkat_pendidikan_id')->references('id')->on('m_tingkat_pendidikans')->onDelete('cascade');
            $table->bigInteger('m_fakultas_id')->unsigned()->nullable();
            $table->foreign('m_fakultas_id')->references('id')->on('m_fakultas')->onDelete('cascade');
            $table->bigInteger('m_jurusan_id')->unsigned()->nullable();
            $table->foreign('m_jurusan_id')->references('id')->on('m_jurusans')->onDelete('cascade');
            $table->bigInteger('m_program_studi_id')->unsigned()->nullable();
            $table->foreign('m_program_studi_id')->references('id')->on('m_program_studis')->onDelete('cascade');
            $table->string('nim');
            $table->string('year_start')->nullable();
            $table->string('year_end')->nullable();
            $table->string('tema_skripsi')->nullable();
            $table->string('judul_skripsi')->nullable();
            $table->longText('deskripsi_skripsi')->nullable();
            $table->double('ipk')->nullable();
            $table->enum('approved_status',['confirmed', 'unconfirmed', 'pending'])->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_education');
    }
}
