<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\ListData\Database\Seeders\ListDataPermissionTableSeeder;
use Modules\Master\Database\Seeders\MasterAddressCitiesTableSeeder;
use Modules\Master\Database\Seeders\MasterAddressProvincesTableSeeder;
use Modules\Master\Database\Seeders\AdminPermissionTableSeeder;
use Modules\Master\Database\Seeders\AdminRoleTableSeeder;
use Modules\Master\Database\Seeders\AdminUserTableSeeder;
use Modules\Master\Database\Seeders\MasterFakultasTableSeeder;
use Modules\Master\Database\Seeders\MasterJabatanTableSeeder;
use Modules\Master\Database\Seeders\MasterJurusanTableSeeder;
use Modules\Master\Database\Seeders\MasterProgramStudiTableSeeder;
use Modules\Master\Database\Seeders\MasterTingkatPendidikanTableSeeder;
use Modules\Master\Database\Seeders\MasterUserTypeTableSeeder;
use Modules\Rbac\Database\Seeders\AdminPermissionTableSeeder as SeedersAdminPermissionTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MasterUserTypeTableSeeder::class);
        $this->call(MasterTingkatPendidikanTableSeeder::class);
        $this->call(MasterFakultasTableSeeder::class);
        $this->call(MasterJurusanTableSeeder::class);
        $this->call(MasterProgramStudiTableSeeder::class);
        $this->call(MasterJabatanTableSeeder::class);
        $this->call(ListDataPermissionTableSeeder::class);

        $this->call(AdminPermissionTableSeeder::class);
        $this->call(SeedersAdminPermissionTableSeeder::class);
        $this->call(AdminRoleTableSeeder::class);
        $this->call(AdminUserTableSeeder::class);
        $this->call(MasterAddressProvincesTableSeeder::class);
        $this->call(MasterAddressCitiesTableSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
