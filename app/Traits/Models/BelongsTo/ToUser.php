<?php

namespace App\Traits\Models\BelongsTo;

use App\Models\User;

trait ToUser
{
    public function approved_user()
    {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function created_user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updated_user()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function deleted_user()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }
}
