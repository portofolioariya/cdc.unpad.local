<?php

namespace App\Traits\Controller\GetData;

use App\Models\User;
use Illuminate\Http\Request;
use Modules\Master\Entities\MJurusan;

trait getDataMaster
{
    public function getJurusan(Request $request)
    {
        # code...
        try {
            //code...
            $jurusans = MJurusan::where('m_fakultas_id', $request->id)->select(['id', 'name'])->get();
            return response()->json($jurusans, 200);
        } catch (\Exception $e) {
            //throw $th;
            return response()->json($e, 500);
        }
    }
}

