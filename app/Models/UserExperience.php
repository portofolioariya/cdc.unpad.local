<?php

namespace App\Models;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\MJabatan;

class UserExperience extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_jabatan_id',
        'nama_perusahaan',
        'tanggal_masuk',
        'tanggal_keluar',
        'description',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }

    public function jabatan()
    {
        return $this->belongsTo(MJabatan::class, 'm_jabatan_id', 'id');
    }

}
