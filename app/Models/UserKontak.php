<?php

namespace App\Models;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserKontak extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'tipe',
        'akun',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];
    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }
}
