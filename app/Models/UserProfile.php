<?php

namespace App\Models;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\MCity;
use Modules\Master\Entities\MGender;
use Modules\Master\Entities\MProvince;

class UserProfile extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_province_id',
        'm_city_id',
        'm_gender_id',
        'images',
        'no_telp',
        'tgl_lahir',
        'address',
        'status_pekerjaan',
        'status_pernikahan',
        'about_me',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }

    public function province()
    {
        return $this->belongsTo(MProvince::class, 'm_province_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(MCity::class, 'm_city_id', 'id');
    }

    public function gender()
    {
        return $this->belongsTo(MGender::class, 'm_gender_id', 'id');
    }
}
