<?php

namespace App\Models;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogActivity extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;
    
    protected $fillable = [
        'user_id',
        'feature',
        'feature_description',
        'status',
        'url_access',
        'method_access',
        'ip_address',
        'user_agent',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    public function access_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
