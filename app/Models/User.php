<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Modules\Master\Entities\MJabatan;
use Modules\Master\Entities\MPerusahaan;
use Modules\Master\Entities\MUserType;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function log_activity() {
        return $this->hasMany(LogActivity::class , 'user_id', 'id');
    }

    public function user_type()
    {
        return $this->morphedByMany(MUserType::class, 'userable');
    }

    public function profile()
    {
        return $this->morphedByMany(UserProfile::class, 'userable');
    }

    public function skill()
    {
        return $this->morphedByMany(UserSkill::class, 'userable');
    }

    public function education()
    {
        return $this->morphedByMany(UserEducation::class, 'userable');
    }

    public function experience()
    {
        return $this->morphedByMany(UserExperience::class, 'userable');
    }

    public function kontak()
    {
        return $this->morphedByMany(UserKontak::class, 'userable');
    }

    public function perusahaan()
    {
        return $this->morphedByMany(MPerusahaan::class, 'userable');
    }

    public function jabatan()
    {
        return $this->morphedByMany(MJabatan::class, 'userable');
    }
}
