<?php

namespace App\Models;

use App\Traits\Models\BelongsTo\ToUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\MFakultas;
use Modules\Master\Entities\MJurusan;
use Modules\Master\Entities\MProgramStudi;
use Modules\Master\Entities\MTingkatPendidikan;

class UserEducation extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ToUser;

    protected $fillable = [
        'm_tingkat_pendidikan_id',
        'm_fakultas_id',
        'm_jurusan_id',
        'm_program_studi_id',
        'nim',
        'year_start',
        'year_end',
        'tema_skripsi',
        'judul_skripsi',
        'deskripsi_skripsi',
        'ipk',
        'approved_status',
        'approved_by',
        'created_by',
        'updated_by',
        'deleted_by',
        'approved_at',
    ];

    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }

    public function tingkat_pendidikan()
    {
        return $this->belongsTo(MTingkatPendidikan::class, 'm_tingkat_pendidikan_id', 'id');
    }

    public function fakultas()
    {
        return $this->belongsTo(MFakultas::class, 'm_fakultas_id', 'id');
    }

    public function jurusan()
    {
        return $this->belongsTo(MJurusan::class, 'm_jurusan_id', 'id');
    }

    public function program_studi()
    {
        return $this->belongsTo(MProgramStudi::class, 'm_program_studi_id', 'id');
    }

}
