<?php
namespace App\Services\Menu;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Modules\Backend\Entities\Services\LogActivity;

class BreadCumb
{
    public static function breadcrumbMenu()
    {
        try {
            //code...
            $routes = explode('/', Request::url());
            $breadcrumb =[];
            $breadcrumb[0]['url'] =  '/';
                $breadcrumb[0]['name'] =  'CDC UNPAD';
                $breadcrumb[0]['status'] = true;
            // dd(count($routes));
            $index = 1;
            if( count($routes) > 3) {
                for ($i=3; $i < count($routes); $i++) { 
                    # code...
                    if (preg_match("/^[a-zA-Z -]+$/",$routes[$i])) {
                        # code...
                        $breadcrumb[$index]['url'] =  $breadcrumb[$index-1]['url'].''.$routes[$i].'/';
                        $breadcrumb[$index]['name'] =  ucwords(str_replace('-', ' ', $routes[$i]));
                        $breadcrumb[$index]['status'] = true;
                        $index++;
                    }else {
                        # code...
                        $breadcrumb[$index]['url'] =  $breadcrumb[$index-1]['url'].''.$routes[$i].'/';
                        $breadcrumb[$index]['name'] =  ucwords(str_replace('-', ' ', $routes[$i]));
                        $breadcrumb[$index]['status'] = false;
                        $index++;
                    }
                }
            }
            return $breadcrumb;
        } catch (\Throwable $th) {
            //throw $th;
            $routes = explode('/', Request::path());
            $breadcrumb = [0=>[
                'url'   => '/backend/',
                'name'  => config('landingpage.company.information.short_name'),
                'status'=> true,
            ],];
            return $breadcrumb;
        }
    }

}