<?php
namespace App\Services\Menu;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Modules\Backend\Entities\Services\LogActivity;

class ActiveMenu
{
    public static function is_active($route = '')
    {
        // try {
            //code...
            $menus = explode('/', Request::path());
            $validate = 0;
            foreach ($menus as $menu) {
                # code...
                if ($menu == $route) {
                    # code...
                    $validate = $validate+1;
                }
            }
            ($validate > 0) ? $status = 'active' : $status ='';    
            return $status;
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     return $status ='';
        // }
    }
}