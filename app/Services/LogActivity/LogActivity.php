<?php
namespace App\Services\LogActivity;

use App\Models\LogActivity as LA;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;


class LogActivity
{

    public static function addToLog($feature, $feature_description ,$status)
    {
        DB::beginTransaction();
        try {
            //code...
            $log = [];
            $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
            $log['feature'] = $feature;
            $log['feature_description'] = $feature_description;
            $log['status'] = $status;
            $log['url_access'] = Request::fullUrl();
            $log['method_access'] = Request::method();
            $log['ip_address'] = Request::ip();
            $log['user_agent'] = Request::header('user-agent');
            $log['created_by'] = auth()->check() ? auth()->user()->id : 1;
            LA::create($log);
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }


    public static function logActivityLists()
    {
        DB::beginTransaction();
        try {
            //code...
            $logs = LA::latest()->get();
            return  $logs;
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }

}
