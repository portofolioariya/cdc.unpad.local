<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\LogActivity\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    public function index($id)
    {
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            //code...
            $user = User::find($id);
            LogActivity::addToLog('profile', 'index', 'success');
            DB::commit();
            return view('profile.profile-detail', compact('user'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog('profile', 'index', 'fail');
            DB::rollback();
            return back();
        }
    }

    public function account_setting($id)
    {
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            //code...
            $user = User::find($id);
            LogActivity::addToLog('profile', 'index', 'success');
            DB::commit();
            return view('profile.account-setting.content.index', compact('user'));
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog('profile', 'index', 'fail');
            DB::rollback();
            return back();
        }
    }
}
