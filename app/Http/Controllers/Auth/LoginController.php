<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\LogActivity\LogActivity;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'login' => 'required',
            'password' => ['required', 'string', 'min:8'],
        ]);

        DB::beginTransaction();
        try {
            $remember = $request->remember ? true : false;
            $fieldType = filter_var($request->login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
            $user = User::where($fieldType, $input['login'])->first();
            // dd($user);
            if (is_null($user)) {
                # code...
                Session::flash('status', 'danger');
                Session::flash('messages', 'Sign in fail user not found!');
                LogActivity::addToLog('Auth', 'Sign in', 'fail');
                DB::rollback();
                return back();
            }
            if(Auth::attempt([$fieldType => $input['login'], 'password' => $input['password']],$remember))
            {
                LogActivity::addToLog('Auth', 'Sign in', 'success');
                DB::commit();
                return redirect('dashboard');
            }else{
                Session::flash('status', 'danger');
                Session::flash('messages', 'Sign in fail password wrong!');
                LogActivity::addToLog('Auth', 'Sign in', 'fail');
                DB::rollback();
                return back();
            }
        } catch (\Throwable $th) {
            //throw $th;
            LogActivity::addToLog('Auth', 'Sign in', 'fail');
            DB::rollback();
            return redirect()->route('login')->with('error','E-mail atau Username & Password salah.');
        }
    }
}
